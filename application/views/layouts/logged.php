<!DOCTYPE html>

<html lang="en "class="no-js">
    <head>
        <meta charset="utf-8"/>
        <title>Amiofeweb</title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/icheck/skins/all.css') ?>" rel="stylesheet"/>
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->

        <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') ?>"/>

        <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/clockface/css/clockface.css') ?>"/>
        <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') ?>"/>
        <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-colorpicker/css/colorpicker.css') ?>"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css') ?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') ?>"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/fullcalendar/fullcalendar.min.css') ?>" rel="stylesheet" type="text/css"/>
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/css/tasks.css') ?>" rel="stylesheet" type="text/css"/>
        <!-- DataTables -->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'jquery-datatables-bs3/assets/css/datatables.css') ?>" rel="stylesheet"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/css/components.css') ?>" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/css/plugins.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/css/layout.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/css/themes/grey.css') ?>" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/css/custom.css') ?>" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
        <link href="<?= $this->config->base_url('public/css/select2-bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url('public/css/select2.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') ?>" rel="stylesheet" type="text/css" />
        <!-- END THEME STYLES -->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/global/plugins/select2/css/select2.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/global/plugins/select2/css/select2-bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/global/css/components.min.css') ?>" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/global/css/plugins.min.css') ?>" rel="stylesheet" type="text/css" />

        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/layouts/layout2/css/layout.min.css') ?>" rel="stylesheet" type="text/css" />
        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/layouts/layout2/css/themes/blue.min.css') ?>" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?= $this->config->base_url(ASSETSPATH . 'teste/layouts/layout2/css/custom.min.css') ?>" rel="stylesheet" type="text/css" />

        <link rel='stylesheet' href='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css") ?>' type='text/css' media='all' />
        <link rel='stylesheet' href='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css?") . time() ?>' type='text/css' media='all' />
        <link rel='stylesheet' href='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css?") . time() ?>' type='text/css' media='all' />
        <link rel='stylesheet' href='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css?") . time() ?>' type='text/css' media='all' />
        <link rel='stylesheet' href='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css?") . time() ?>' type='text/css' media='all' />
        <link rel="stylesheet" href="<?= $this->config->base_url(ASSETSPATH . 'pnotify/pnotify.custom.css') ?> " />


        <!-- END THEME LAYOUT STYLES -->

        <!-- BEGIN CORE PLUGINS -->
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery-migrate.min.js') ?>" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->

        <!-- DataTable -->
        <script src="<?= $this->config->base_url(ASSETSPATH . 'jquery-datatables/media/js/jquery.dataTables.js') ?>"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') ?>"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'jquery-datatables-bs3/assets/js/datatables.js') ?>"></script>
        <!-- Examples -->
        <script src="<?= $this->config->base_url(ASSETSPATH . 'tables/examples.datatables.tabletools.js') ?>"></script>  

        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery-ui/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery.cokie.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->

        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/icheck/icheck.min.js') ?>"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/scripts/form-icheck.js') ?>"></script>
        <script type="text/javascript" src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') ?>"></script>

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/flot/jquery.flot.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/flot/jquery.flot.resize.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/flot/jquery.flot.categories.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery.pulsate.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-daterangepicker/moment.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-daterangepicker/daterangepicker.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/clockface/js/clockface.js"') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') ?>" type="text/javascript"></script>

        <!-- IMPORTANT! fullcalendar depends on jquery-ui.min.js for drag & drop support -->
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/fullcalendar/fullcalendar.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery.sparkline.min.js') ?>" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/scripts/metronic.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/scripts/layout.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/scripts/quick-sidebar.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/scripts/demo.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/scripts/form-validation.js') ?>"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>

        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/scripts/index.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/scripts/tasks.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(JSPATH . 'home.js') ?>" type="text/javascript"></script>

        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net/js/jquery.dataTables.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-buttons/js/dataTables.buttons.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-buttons/js/buttons.flash.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-buttons/js/buttons.html5.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-buttons/js/buttons.print.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-responsive/js/dataTables.responsive.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/datatables.net-scroller/js/datatables.scroller.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/jszip/dist/jszip.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/pdfmake/build/pdfmake.min.js?") . time() ?>'></script>
        <script type='text/javascript' src='<?= $this->config->base_url(ADMINPATH . "/vendors/pdfmake/build/vfs_fonts.js?") . time() ?>'></script>

        <script src="<?= $this->config->base_url(ASSETSPATH . 'sweetalert/dist/sweetalert2.all.min.js') ?>" type="text/javascript"></script>
        <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'sweetalert/dist/sweetalert2.min.js') ?>" type="text/javascript"></script>

        <link rel="stylesheet" href="<?= $this->config->base_url(ASSETSPATH . 'sweetalert/dist/sweetalert2.min.css') ?> " />

        <!-- END PAGE LEVEL SCRIPTS -->
        <script>
            jQuery(document).ready(function () {
                Metronic.init(); // init metronic core componets
                Layout.init(); // init layout
                Demo.init(); // init demo features 
                QuickSidebar.init(); // init quick sidebar
                Index.init();
                Index.initDashboardDaterange();
                Index.initCalendar(); // init index page's custom scripts
                Index.initCharts(); // init index page's custom scripts
                Index.initChat();
                Index.initMiniCharts();
                Tasks.initDashboardWidget();
                FormiCheck.init(); // init page demo
            });
        </script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'pnotify/pnotify.custom.js') ?>"></script>

        <script src="<?= $this->config->base_url(ASSETSPATH . 'teste/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'teste/global/plugins/select2/js/select2.full.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'teste/global/scripts/app.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'teste/pages/scripts/components-select2.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'teste/layouts/layout2/scripts/layout.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'teste/layouts/layout2/scripts/demo.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'teste/layouts/global/scripts/quick-sidebar.min.js') ?>" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->

        <!-- END JAVASCRIPTS -->
    </head>
    <body data-baseurl="<?= $this->config->base_url(); ?>" class="page-boxed page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo">
        <?php $this->user = $this->session->userdata('user_amiofe'); ?>
        <div class="page-header navbar navbar-fixed-top">
            <div class="page-header-inner container">
                <div class="page-logo">
                    <a href="<?= $this->config->base_url('home/index') ?>">
<!--                        <img src="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/img/logo-default.png') ?>" alt="logo" class="logo-default"/>-->
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                    </div>
                </div>
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                </a>
                <div class="page-top">
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false">
                                    <span class="username username-hide-on-mobile">
                                        <?= $this->user['name'] ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="<?= $this->config->base_url('home/edit_user') . '/' . $this->user['id'] ?>" data-target="#ajax" data-toggle="modal">
                                            <i class="icon-user"></i> Meu perfil
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?= $this->config->base_url('Login/logout') ?>">
                                            <i class="icon-key"></i> Sair </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix">
        </div>
        <div class="container">
            <div class="page-container">
                <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                            <li class="start active ">
                                <a href="<?= $this->config->base_url('home/new_test') ?>">
                                    <i class="icon-plus"></i>
                                    <span class="title">Nova Avaliação</span>
                                    <span class="selected"></span>
                                </a>
                            </li>
                            <li class="start ">
                                <a href="<?= $this->config->base_url('home/list_tests') ?>">
                                    <i class="fa fa-check"></i>
                                    <span class="title">Avaliações</span>
                                </a>
                            </li>
                            <li class="start ">
                                <a href="<?= $this->config->base_url('home/list_patients') ?>">
                                    <i class="icon-users"></i>
                                    <span class="title">Indivíduos</span>
                                </a>
                            </li>
                            <li class="start ">
                                <a href="<?= $this->config->base_url('home/list_users') ?>">
                                    <i class="icon-users"></i>
                                    <span class="title">Usuários</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <?php $this->load->view($content) ?>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="ajax" role="basic" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <img src="<?= $this->config->base_url(ASSETSPATH . 'global/img/loading-spinner-grey.gif') ?>" alt="" class="loading">
                            <span>
                                &nbsp;&nbsp;Carregando... </span>
                        </div>
                    </div>
                </div>
            </div>

    </body>
</html>