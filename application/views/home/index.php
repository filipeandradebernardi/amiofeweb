<!--
<h3 class="page-title">
    AMIOFEWEB <small>Sobre nós</small>
</h3>

<div class="portlet light">
    <div class="portlet-body">
        <div class="row margin-bottom-30">
            <div class="col-md-6">
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.
                </p>
                <ul class="list-unstyled margin-top-10 margin-bottom-10">
                    <li>
                        <i class="fa fa-check icon-default"></i> Nam liber tempor cum soluta
                    </li>
                    <li>
                        <i class="fa fa-check icon-success"></i> Mirum est notare quam
                    </li>
                    <li>
                        <i class="fa fa-check icon-info"></i> Lorem ipsum dolor sit amet
                    </li>
                    <li>
                        <i class="fa fa-check icon-danger"></i> Mirum est notare quam
                    </li>
                    <li>
                        <i class="fa fa-check icon-warning"></i> Mirum est notare quam
                    </li>
                </ul>
                 Blockquotes 
                <blockquote class="hero">
                    <p>
                        Lorem ipsum dolor sit amet, consectetuer sed diam nonummy nibh euismod tincidunt.
                    </p>
                    <small>Bob Nilson</small>
                </blockquote>
            </div>
            <div class="col-md-6">
                <iframe src="http://player.vimeo.com/video/22439234" style="width:100%; height:327px;border:0" allowfullscreen>
                </iframe>
            </div>
        </div>
        /row
         Meer Our Team 
        <div class="headline">
            <h3>Meet Our Team</h3>
        </div>
        <div class="row thumbnails">
            <div class="col-md-3">
                <div class="meet-our-team">
                    <h3>Bob Nilson <small>Chief Executive Officer / CEO</small></h3>
                    <img src="../../assets/admin/pages/media/pages/2.jpg" alt="" class="img-responsive"/>
                    <div class="team-info">
                        <p>
                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                        </p>
                        <ul class="social-icons pull-right">
                            <li>
                                <a href="javascript:;" data-original-title="twitter" class="twitter">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="facebook" class="facebook">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="linkedin" class="linkedin">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="skype" class="skype">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="meet-our-team">
                    <h3>Marta Doe <small>Project Manager</small></h3>
                    <img src="../../assets/admin/pages/media/pages/3.jpg" alt="" class="img-responsive"/>
                    <div class="team-info">
                        <p>
                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                        </p>
                        <ul class="social-icons pull-right">
                            <li>
                                <a href="javascript:;" data-original-title="twitter" class="twitter">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="facebook" class="facebook">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="linkedin" class="linkedin">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="skype" class="skype">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="meet-our-team">
                    <h3>Bob Nilson <small>Chief Executive Officer / CEO</small></h3>
                    <img src="../../assets/admin/pages/media/pages/2.jpg" alt="" class="img-responsive"/>
                    <div class="team-info">
                        <p>
                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                        </p>
                        <ul class="social-icons pull-right">
                            <li>
                                <a href="javascript:;" data-original-title="twitter" class="twitter">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="facebook" class="facebook">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="linkedin" class="linkedin">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="skype" class="skype">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="meet-our-team">
                    <h3>Marta Doe <small>Project Manager</small></h3>
                    <img src="../../assets/admin/pages/media/pages/3.jpg" alt="" class="img-responsive"/>
                    <div class="team-info">
                        <p>
                            Donec id elit non mi porta gravida at eget metus. Fusce dapibus, justo sit amet risus etiam porta sem...
                        </p>
                        <ul class="social-icons pull-right">
                            <li>
                                <a href="javascript:;" data-original-title="twitter" class="twitter">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="facebook" class="facebook">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="linkedin" class="linkedin">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="Goole Plus" class="googleplus">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" data-original-title="skype" class="skype">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


-->
