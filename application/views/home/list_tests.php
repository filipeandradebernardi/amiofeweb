<link href="<?= $this->config->base_url(CSSPATH . 'dropzone/dropzone.css') ?>" rel="stylesheet">

<h3 class="page-title">Avaliações </h3>


<?php $this->user = $this->session->userdata('user_amiofe') ?>

<div id="div_upload" hidden="">
    <section class="panel">
        <div class="panel-body">
            <form action="<?= $this->config->base_url('home/receive_video/') ?>" class="dropzone dz-square" id="dropzone" style="height: 100%;">
                <input id="test_id" name="test_id" value="" hidden=""/>
            </form></br>
            <a href="javascript:void(0)" onclick="close_upload_view()" class="btn btn-danger pull-right"> Fechar</a>
        </div>
    </section>
</div>

<div id="view_file_div" hidden="">
    <section class="panel">
        <div class="panel-body">
            <div id="view_file">
            </div>
            <a href="javascript:void(0)" onclick="close_upload_view()" class="btn btn-danger pull-right"> Fechar</a>
        </div>
    </section>
</div>



<section class="panel">
    <div class="panel-body">
<!--        <a href="<?= $this->config->base_url('home/new_test') ?>" class="btn btn-success btn-lg pull-left"><i class="fa fa-plus-circle"></i> Adicionar</a>-->

        <table class="table table-bordered table-striped mb-none" id="table_eval" data-swf-path="<?= $this->config->base_url(ASSETSPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>">
            <thead>
                <tr>

                    <th>Avaliação</th>
                    <th>Indivíduo</th>
                    <?php if (isset($tests[0])): ?>
                        <th>Data da avaliação</th>
                        <th>Data de Inserção</th>
                        <th>Avaliador</th>
                        <th>Escore Total</th>
    <!--                        <th>Grupo</th>-->
                        <th>Ações</th>
                    <?php endif; ?>
                </tr>
            </thead>

            <tbody>
                <?php if (isset($tests)): ?>
                    <?php foreach ($tests as $p): ?>

                        <tr class="content_row">
                            <td><?= $p['id'] ?></td>
                            <td><?= ($p['patient_name']) ?></td>
                            <td><?= $p['evaldate'] ?></td>
                            <td><?= $p['realization_date'] ?></td>
                            <td><?= $p['user_name'] ?></td>
                            <td><?= $p['tab1_resultgeral'] ?></td>
        <!--                            <td><?= $p['group'] ?></td>-->
                            <td>
                                <?php if ($this->user['id'] == $p['user_id']): ?>
                                    <a href="<?= $this->config->base_url('home/view_test/' . $p['id']) ?>" title="Visualizar avaliação"><i class="fa fa-eye"></i></a>
                                    <a href="<?= $this->config->base_url('home/edit_test/' . $p['id']) ?>" title="Editar avaliação"><i class="fa fa-pencil-square-o"></i></a>
                                    <a href="javascript:void(0)" onclick="upload_view(<?= $p['id'] ?>)" title="Subir video"><i class="fa fa-cloud-upload"></i></a>
                                    <a href="javascript:void(0)" onclick="view_video(<?= $p['id'] ?>)" title="Ver video"><i class="fa fa-play-circle-o"></i></a>
                                    <a href="<?= $this->config->base_url('home/show_graph/' . $p['id']) ?>" title="Gerar gráfico"><i class="fa fa-bar-chart"></i></a>
                                    <a href="javascript:void(0)" onclick="return confirm('Certeza que deseja excluir esta avaliação?') ? delete_test(<?= $p['id'] ?>) : '';" title="Deletar avaliação"><i class="fa fa-trash-o"></i></a>

                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</section>

<script src='<?= $this->config->base_url(JSPATH . 'dropzone.min.js') ?>'></script>
<script>

                                        function view_video(test_id) {
                                            jQuery('#view_file').html('<embed src="' + jQuery("body").data("baseurl") + 'home/get_video_test/' + test_id + '" width="100%" height="500px" controls="false"> </embed>');
                                            jQuery('#view_file_div').show();
                                        }

                                        function upload_view(test_id) {
                                            jQuery('#div_upload').slideDown();
                                            jQuery('#test_id').val(test_id);
                                        }

                                        function close_upload_view() {
                                            jQuery('#div_upload').slideUp();
                                            jQuery('#view_file_div').slideUp();
                                            jQuery('#test_id').val('');
                                        }

                                        function delete_test(id) {
                                            jQuery.ajax({
                                                url: jQuery("body").data("baseurl") + "home/delete_test",
                                                type: "post",
                                                dataType: 'json',
                                                data: {
                                                    id: id
                                                },
                                                success: function (response) {
                                                    if (response.status == "OK") {
                                                        window.location = jQuery("body").data('baseurl') + 'home/list_tests';
                                                    }
                                                }
                                            });
                                        }

                                        function edit_test(id) {
                                            jQuery.ajax({
                                                url: jQuery("body").data("baseurl") + "home/edit_test",
                                                type: "post",
                                                dataType: 'json',
                                                data: {
                                                    id: id
                                                },
                                                success: function (response) {
                                                    if (response.status == "OK") {
                                                        window.location = jQuery("body").data('baseurl') + 'home/list_tests';
                                                    }
                                                }
                                            });
                                        }




                                        Dropzone.autoDiscover = false; // otherwise will be initialized twice
                                        var myDropzoneOptions = {
                                            maxFiles: 10,
                                            maxFilesize: 500000000000000,
                                            acceptedMimeTypes: 'video/*',
                                            init: function () {
                                                this.on("complete", function (file) {
                                                    //window.location.reload(true);
                                                });
                                            }
                                        };
                                        var myDropzone = new Dropzone('#dropzone', myDropzoneOptions);

</script>
<script>
    $(document).ready(function () {
        $('#table_eval').DataTable({
//            "scrollY": 400,
            "scrollX": true,
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Nenhum registro encontrado",
                "info": "Exibindo página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhuma informação preenchida",
                "infoFiltered": "(filtrados de _MAX_ total registros)"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tudo"]],
            "searching": true,
            "dom": '<"top"ft>rt<"bottom"il><"clear"p>'

        });
    });
</script>