<?php $this->user = $this->session->userdata('user_amiofe'); ?>
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Editar usuário</h4>
</div>
<div class="modal-body">
    <form role="form">
        <div class="form-body">
            <div class="form-group email">
                <label>Email*</label>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                    </span>
                    <input type="text" id="email" class="form-control input-lg" placeholder="Email" value="<?= $user['email'] ?>">
                </div>
            </div>
            <div class="form-group password">
                <label for="exampleInputPassword1">Senha*</label>
                <div class="input-group">
                    <input type="password" class="form-control input-lg" id="password" placeholder="Senha" value="<?= $user['password'] ?>">
                    <span class="input-group-addon">
                        <i class="fa fa-key"></i>
                    </span>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6 name">
                    <label>Nome*</label>
                    <div class="input-icon input-icon-lg">
                        <i class="fa fa-user"></i>
                        <input type="text" id="name" class="form-control input-lg" placeholder="Nome" value="<?= $user['name'] ?>">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label>Instituição</label>
                    <select class="form-control input-lg" id="institution">
                        <option value="usp" <?php
                        if ($user['institution'] == 'usp'): echo 'selected';
                        endif;
                        ?>>Usp</option>
                        <option value="other" <?php
                        if ($user['institution'] == 'other'): echo 'selected';
                        endif;
                        ?>>Outra</option>
                    </select>
                </div>
                <?php if ($this->user['permission'] == 'su'): ?>
                    <div class="form-group col-md-6">
                        <label>Permissão</label>
                        <select class="form-control input-lg" id="permission">
                            <option value="default" <?php
                            if ($user['permission'] == 'default'): echo 'selected';
                            endif;
                            ?>>Usuário comum</option>
                            <option value="su" <?php
                            if ($user['permission'] == 'su'): echo 'selected';
                            endif;
                            ?>>Super usuário</option>
                        </select>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Fechar</button>
    <a href="javascript:void(0)" onclick="save_edited_user(<?= $user['id'] ?>)" class="btn blue">Salvar</a>
</div>
