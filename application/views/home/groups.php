<link href="<?= $this->config->base_url(CSSPATH . 'dropzone/dropzone.css') ?>" rel="stylesheet">

<h3 class="page-title"><?= $title ?></h3>

<?php $this->user = $this->session->userdata('user_amiofe') ?>

<section class="panel">
    <div class="panel-body">
        <a href="<?= $this->config->base_url('home/new_subgroup') ?>" class="btn btn-success btn-lg"><i class="fa fa-plus-circle"></i> Adicionar Subgrupo</a>
        <a href="<?= $this->config->base_url('home/patient_group') ?>" class="btn btn-success btn-lg "><i class="fa fa-plus-circle"></i> Classificar Teste</a></h3>

        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="<?= $this->config->base_url(ASSETSPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Grupo</th>
                    <th>Subgrupo</th>
                    <th>Criado por</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php if (isset($groups)): ?>
                    <?php foreach ($groups as $p): ?>
                        <tr class="content_row">
                            <td><?= $p['id'] ?></td>
                            <td><?= $p['category_group'] ?></td>
                            <td><?= $p['subgroup_name'] ?></td>
                            <td><?= $p['name_user'] ?></td>
                            <td>
                                <a href="javascript:void(0)"title="Deletar subgrupo" onclick="delete_subgroup(<?= $p['id'] ?>)"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</section>

<script>

    function delete_subgroup(id) {
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "home/delete_subgroup",
            type: "post",
            dataType: 'json',
            data: {
                id: id
            },
            success: function (response) {
                if (response.status == "OK") {
                    window.location = jQuery("body").data('baseurl') + 'home/groups';
                }
            }
        });
    }
</script>