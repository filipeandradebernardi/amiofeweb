<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus-circle"></i> Adicionando novo Subgrupo
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form">
            <div class="form-body">
                <div class="row">
                    <div class="form-group col-md-6 name">
                        <label>Nome do Subgrupo:</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-group"></i>
                            <input type="text" id="subgroup_name" class="form-control input-lg" placeholder="Nome do SubGrupo">
                        </div>
                    </div>
                    <div class="form-group col-md-6 name">
                        <label>Categoria do Subgrupo:</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-group"></i>
                        <select class="form-control input-lg" id="category_group">
                            <option value="Controle">1 - Controle</option>
                            <option value="Não Controle">2 - Não Controle</option>
                           <option value="Excluidos">3 - Excluidos</option>
                        </select>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="form-actions">
                <a href="javascript:void(0)" onclick="save_subgroup()" class="btn blue">Salvar</a>
            </div>
        </form>
    </div>
</div>
<script src="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/scripts/components-pickers.js') ?>"></script>
<script>
                    jQuery(document).ready(function () {
                        ComponentsPickers.init();
                    });


                    function save_subgroup() {

                        if (!jQuery("#subgroup_name").val()) {
                            jQuery('#subgroup_name').addClass('has-error');
                            return false;
                        }
                        if (!jQuery("#category_group").val()) {
                            jQuery('#category_group').addClass('has-error');
                            return false;
                        }
                        jQuery.ajax({
                            url: jQuery("body").data("baseurl") + "home/save_subgroup",
                            type: "post",
                            dataType: 'json',
                            data: {
                                subgroup_name: jQuery("#subgroup_name").val(),
                                category_group:jQuery("#category_group").val()
                                
                            },
                            success: function (response) {
                               
                                if (response.status == "OK") {
                                    window.location = jQuery("body").data('baseurl') + 'home/groups';
                                }
                            }
                        });

                    }
</script>