<head>
    <script src="<?= $this->config->base_url(ASSETSPATH . 'chart/samples/bundle.js') ?>" type="text/javascript"></script>
    <script src="<?= $this->config->base_url(ASSETSPATH . 'chart/samples/utils.js') ?>" type="text/javascript"></script>
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
</head>
<div class="panel-body"> 


    <div  class='col-sm-9'>
        <h4>Total</h4>
        <canvas id="canvas4"></canvas>
    </div>

    <div class="col-md-12">
        <div class='col-sm-6'>
            <h4>Aparência e condição postural/posição</h4>
            <canvas id="acpp"></canvas>
        </div>
        <div class='col-sm-6'>
            <h4>Mobilidade</h4>
            <canvas id="mob"></canvas>
        </div>
    </div>
    <div class="col-md-12">
        <div id="container3" class='col-sm-6'>
            <h4>Funções</h4>
            <canvas id="canvas3"></canvas>
        </div>

    </div>


</div>
<script>
    var color = Chart.helpers.color;
    var barChartData_tabacpp = {
        labels: <?php print_r($title_tab1); ?>,
        datasets: [{
                label: 'Valor de Referência',
                backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                borderColor: window.chartColors.blue,
                borderWidth: 1,
                data: [3]


            }, {
                label: 'Sujeito',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: <?php print_r($value_tab1); ?>
            }]
    };
    var barChartData_tabmob = {
        labels: <?php print_r($title_tab2); ?>,
        datasets: [{
                label: 'Valor de Referência',
                backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: <?php print_r($value_referencia_mob); ?>

            }, {
                label: 'Sujeito',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: <?php print_r($value_tab2); ?>
            }]
    };
    var barChartData_tab3 = {
        labels: <?php print_r($title_tab3); ?>,
        datasets: [{
                label: 'Valor de Referência',
                backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: [3]


            }, {
                label: 'Sujeito',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: <?php print_r($value_tab2); ?>
            }]
    };

    var barChartData_tabtotal = {
        labels: <?php print_r($title_total); ?>,
        datasets: [{
                label: 'Valor de Referência',
                backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: <?php print_r($value_referencia); ?>


            }, {
                label: 'Sujeito',
                backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
                borderColor: window.chartColors.red,
                borderWidth: 1,
                data: <?php print_r($value_total); ?>
            }
        ]
    };


    window.onload = function () {
        var ctx = document.getElementById("acpp").getContext("2d");
        var dtx = document.getElementById("mob").getContext("2d");
//        var etx = document.getElementById("canvas3").getContext("2d");
        var ftx = document.getElementById("canvas4").getContext("2d");
        //accp
        window.myBar_tab1 = new Chart(ctx, {
            type: 'bar',
            data: barChartData_tabacpp,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                }

            }
        });
        //mob
        window.myBar_tab2 = new Chart(dtx, {
            type: 'bar',
            data: barChartData_tabmob,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                }
            }
        });
//        window.myBar_tab3 = new Chart(etx, {
//            type: 'horizontalBar',
//            data: barChartData_tab3,
//            options: {
//                responsive: true,
//                legend: {
//                    position: 'top',
//                }
//            }
//        });

        //geral
        window.myBar_tab4 = new Chart(ftx, {
            type: 'bar',
            data: barChartData_tabtotal,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                }
            }
        });


    };


</script>
