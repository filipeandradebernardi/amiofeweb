<?php if (isset($teste)): ?>
    <input id="test_id" hidden value="<?= $teste['id'] ?>"  >
    <?php
else:
    $teste = null;
endif;
?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue" id="form_wizard_1">
            <!-- Referência do Protocolo-->
            <div class="portlet-title">
                <div class="caption">
                    <a href="#" class="dcontexto"> Protocolo de avaliação miofuncional orofacial com escores (AMIOFE)
                        <span>Referência:
                            <p class="formata">-Felício CM, Ferreira CL. Protocol of orofacial myofunctional evaluation with scores. Int J Pediatr Otorhinolaryngol. 2008; 72:367-375. (validado para crianças).<br>
                                -De Felício CM, Medeiros AP, de Oliveira Melchior M. Validity of the 'protocol of oro-facial myofunctional evaluation with scores' for young and adult subjects. J Oral Rehabit. 2012;39:744-753. (validado para jovens e adultos)
                            </p>
                        </span></a> 
                </div>
            </div>
            <div class="portlet-body form">
                <div action="#" class="form-horizontal" id="submit_form" method="POST">
                    <div class="form-wizard">
                        <div class="form-body">
                            <!-- lista de etapas -->
                            <ul class="nav nav-pills nav-justified steps">
                                <li class="active">
                                    <a href="#tab1" data-toggle="tab" class="step active"id='bntetp1'>
                                        <span class="number">
                                            1 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i>Informações do Indivíduo </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab2" data-toggle="tab" class="step"id='bntetp2'>
                                        <span class="number">
                                            2 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Aparência e condição postural/posição </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab3" data-toggle="tab" class="step" id='bntetp3'>
                                        <span class="number">
                                            3 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Mobilidade </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab4" data-toggle="tab" class="step"id='bntetp4'>
                                        <span class="number">
                                            4 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Funções </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab5" data-toggle="tab" class="step"id='bntetp5'>
                                        <span class="number">
                                            5 </span>
                                        <span class="desc">
                                            <i class="fa fa-check"></i> Análise da oclusão </span>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <!--Etapa 1 - Dados do indivíduo -->
                                <div class="tab-pane active" id="tab1">
                                    <form role="form" id="start_eval">
                                        <h4 class="form-section"><b>Dados do Indivíduo</b></h4>
                                        <div class='form-group'>
                                            <div class="input-icon input-icon-lg">
                                                <div class='col-md-2'>
                                                    <h5 class="form-section">Adicionar indivíduo</h5> 
                                                    <a href="<?= $this->config->base_url('home/new_patient') ?>" title='Adicionar novo Indivíduo'class="btn btn-success btn-sm">
                                                        Novo Indivíduo <i class="fa fa-plus-circle fa-lg"></i>
                                                    </a>
                                                </div>
                                                <div class='col-md-6'>
                                                    <h5 class="form-section"style="text-align:center;">Lista de indivíduos</h5> 
                                                    <div class="input-group select2-bootstrap-prepend">
                                                        <span class="input-group-addon">
                                                            <select id="patient_id" class="form-control select2-allow-clear">
                                                                <?php if (isset($teste['name'])): ?>
                                                                    <option value="<?= $teste['patient_id'] ?>"><?= ($teste['name']) ?></option>
                                                                    <?php
                                                                else:
                                                                    foreach ($patients as $p):
                                                                        ?>
                                                                        <option></option> 
                                                                        <option value="<?= $p['id'] ?>"><?= ($p['name']) ?></option>
                                                                        <?php
                                                                    endforeach;
                                                                endif;
                                                                ?>
                                                            </select>
                                                        </span>

                                                    </div>
                                                </div>
                                                <div class='col-md-4'>
                                                    <h5 class="form-section">Data da avaliação</h5> 

                                                    <?php if (isset($teste['evaldate'])): ?>
                                                        <div class="input-group date " data-date-format="yyyy-mm-dd">
                                                            <input type="text" class="form-control input-sm" readonly id="evaldate" value="<?= $teste['evaldate'] ?>">

                                                        </div>
                                                    <?php else: ?>
                                                        <div class="form-group col-md-2">
                                                            <div class="input-group date " data-date-format="yyyy-mm-dd">
                                                                <input type="date" class='form-control input-lg' id="evaldate" value="<?= $teste['evaldate'] ?>">
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>

                                                </div>
                                            </div>
                                        </div>

                                        <h4 class="form-section"><b>Dados da Avaliação</b></h4>
                                        <div class='form-group'>
                                            <div class="input-icon input-icon-lg">
                                                <div class='col-md-6'>
                                                    <h5 class="form-section"style="text-align:center;">Início do problema</h5> 
                                                    <textarea class='form-control' rows='3' name="startproblem" value="<?= $teste['startproblem'] ?>"  class="icheck" data-radio="iradio_flat-blue"><?= $teste['startproblem'] ?> </textarea>
                                                </div>
                                                <div class='col-md-6'>
                                                    <h5 class="form-section"style="text-align:center;">Queixa</h5> 
                                                    <textarea class='form-control' rows='3' name="queixa" value="<?= $teste['queixa'] ?>"  class="icheck" data-radio="iradio_flat-blue"><?= $teste['queixa'] ?> </textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-section">
                                            <a href="#tab2"  onclick="muda_botao('bntetp2')" data-toggle="tab" class="btn blue"style="float:right;">
                                                Seguinte
                                            </a>
                                        </div>
                                    </form>
                                </div>
                                <!--Etapa 2 - aparência e condiçao postural -->
                                <div class="tab-pane" id="tab2">
                                    <form role="form" id='acpp'>
                                        <h3 class="block"><b>Aparência e condição postural/posição</b></h3>
                                        <br>
                                        <!--Aparência e condição postural/posição -->
                                        <div>
                                            <h4 class="form-section">Condicão postural dos lábios</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Oclusão normal dos lábios
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa1" value="3" <?php if ($teste['radioa1'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Normal
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Oclusão dos lábios com tensão
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input id="radioa1_aa" type="radio" name="radioa1" value="2" <?php if (($teste['radioa1'] == 2) && ($teste['radioa1_radiovalue'] == 'atividade_aumentada')) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Atividade aumentada dos lábios e músculo Mentual
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Ausência de oclusão labial
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input id="radioa1_dl" type="radio" name="radioa1" value="2"<?php if (($teste['radioa1'] == 2) && ($teste['radioa1_radiovalue'] == 'disfuncao_leve')) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Disfunção leve

                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radioa1" value="1"<?php if ($teste['radioa1'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Disfunção severa
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section">Postura vertical da mandíbula</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Postura normal
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa2" value="3"<?php if ($teste['radioa2'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Mantem espaço funcional livre
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Oclusão dos dentes
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input id="radioa2_se" type="radio" name="radioa2" value="2"<?php if (($teste['radioa2'] == 2) && ($teste['radioa2_radiovalue'] == 'sem_espaco')) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Sem espaço funcional livre
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Boca aberta
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input id="radioa2_dl" type="radio" name="radioa2" value="2"<?php if (($teste['radioa2'] == 2) && ($teste['radioa2_radiovalue'] == 'disfuncao_leve')) echo 'checked'; ?>class="icheck" data-radio="iradio_flat-blue"> (2) Disfunção leve
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Excessiva abertura da boca
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa2" value="1"<?php if ($teste['radioa2'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Disfunção severa
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section">Aparência das bochechas</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Normal
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa3" value="3"<?php if ($teste['radioa3'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3)
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Volume aumentado
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input id="radioa3_lv" type="radio" name="radioa3" value="2"<?php if ($teste['radioa3_radiovalue'] == 'leve_volume') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Leve
                                                            </label>
                                                            <label>
                                                                <input id="radioa3_sv" type="radio" name="radioa3" value="1" <?php if ($teste['radioa3_radiovalue'] == 'severa_volume') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Severa
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" hidden id="radioa3_volume_aumentado">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Lado maior
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa3_volume_aumentado" value="direito"<?php if ($teste['radioa3_volume_aumentado'] == 'direito') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Direito
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radioa3_volume_aumentado" value="esquerdo"<?php if ($teste['radioa3_volume_aumentado'] == 'esquerdo') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Esquerdo
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Flácida / Arqueada
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input id="radioa3_lf" type="radio" name="radioa3" value="2" <?php if (($teste['radioa3'] == 2) && ($teste['radioa3_radiovalue'] == 'leve_flacida')) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Leve
                                                            </label>
                                                            <label>
                                                                <input id="radioa3_sf" type="radio" name="radioa3" value="1" <?php if (($teste['radioa3'] == 1) && ($teste['radioa3_radiovalue'] == 'severa_flacida')) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Severa
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section">Aparência da face</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Simetria entre os lábios direito e esquerdo
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa4" value="3"<?php if ($teste['radioa4'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Normal
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Assimetria
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa4" value="2"<?php if ($teste['radioa4'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Leve
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radioa4" value="1"<?php if ($teste['radioa4'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Severa
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="radioa4_lado_aumentado" hidden>
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Lado maior
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa4_radiovalue" value="direito"<?php if ($teste['radioa4_radiovalue'] == 'direito') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Direito
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radioa4_radiovalue" value="esquerdo"<?php if ($teste['radioa4_radiovalue'] == 'esquerdo') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Esquerdo
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section">Posição da língua</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Contida na cavidade oral
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa5" value="3"<?php if ($teste['radioa5'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Normal
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Interposta aos arcos dentário
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa5" value="2"<?php if ($teste['radioa5'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Adaptação ou disfunção
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radioa5" value="1"<?php if ($teste['radioa5'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Protruída em excesso
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group"hidden id="radioa5_local">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Local
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa5_local" value="anterior"<?php if ($teste['radioa5_local'] == 'anterior') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Anterior
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radioa5_local" value="lateral_posterior"<?php if ($teste['radioa5_local'] == 'lateral_posterior') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Lateral/Posterior
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radioa5_local" value="toda_extensao"<?php if ($teste['radioa5_local'] == 'toda_extensao') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Toda a extensão do arco
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section">Aparência do palato duro</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Normal
                                                </label>   
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa6" value="3"<?php if ($teste['radioa6'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Largura diminuida (estreito)
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radioa6" value="2"<?php if ($teste['radioa6'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Leve
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radioa6" value="1" <?php if ($teste['radioa6'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Severo
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="form-section">Observações - Aparência e condição postural/posição</h4>
                                            <div class='form-group'>
                                                <div class="input-icon input-icon-lg">
                                                    <div class='col-md-12'>
                                                        <textarea class='form-control' rows='3' name="observacoes_acpp" value="<?= $teste['observacoes_acpp'] ?>"  class="icheck" data-radio="iradio_flat-blue"><?= $teste['observacoes_acpp'] ?> </textarea>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <br>
                                        <h4 class="form-section"><b>Total Aparência e condição postural/posição</b> 
                                            <label id="tab1_result" class="result_tabs" style="float: right; font-size: 15pt">
                                                <?php if (isset($teste['tab1_result'])): ?>
                                                    <?= ($teste['tab1_result']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>

                                            </label>
                                        </h4>
                                        <h4 class="form-section"><b>Total geral </b> 
                                            <label id="result_geral" class="tab1_resultgeral" style="float: right; font-size: 15pt">
                                                <?php if (isset($teste['tab1_resultgeral'])): ?>
                                                    <?= ($teste['tab1_resultgeral']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>
                                            </label></h4>
                                        <div class="form-section">
                                            <a href="#tab3"  onclick="muda_botao('bntetp3')" data-toggle="tab" class="btn blue"style="float:right;">
                                                Seguinte
                                            </a>
                                            <a href="#tab1"  onclick="muda_botao('bntetp1')" data-toggle="tab" class="btn blue"style="float:left;">
                                                Voltar
                                            </a>
                                        </div>
                                    </form>
                                </div>                                
                                <!--Etapa 3 - Mobilidade -->
                                <div class="tab-pane" id="tab3">
                                    <form role="form" id='mob'> 
                                        <h3 class="block"><b>Mobilidade</b></h3>
                                        <h4 class="form-section">Movimentos labiais <label style="float: right; font-size: 15pt" id="tab2_1_result" class="result_tabs">
                                                <?php if (isset($teste['tab2_1_result'])): ?>
                                                    <?= ($teste['tab2_1_result']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>  
                                            </label></h4>
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            Desempenho
                                                        </label>
                                                        <label>
                                                            Preciso
                                                        </label>
                                                        <label>
                                                            Falta de precisão/tremor
                                                        </label>
                                                        <label>
                                                            Inabilidade severa
                                                        </label>
                                                        <div id="especifique_radioma" hidden>
                                                            <label>
                                                                Especifique:
                                                            </label></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Protrusão</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radioma1" value="3"<?php if ($teste['radioma1'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radioma1" value="2" <?php if ($teste['radioma1'] == 2) echo 'checked'; ?>class="icheck" data-radio="iradio_flat-blue"> (2) </label>

                                                        <label>
                                                            <input type="radio" name="radioma1" value="1" <?php if ($teste['radioma1'] == 1) echo 'checked'; ?>class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radioma1" hidden>
                                                        <select class="form-control input-sm" id="especifique_radioma1_val">
                                                            <?php if (isset($teste['especifique_radioma1_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radioma1_val']) ?>"><?= ($teste['especifique_radioma1_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Retração</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radioma2" value="3"<?php if ($teste['radioma2'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radioma2" value="2"<?php if ($teste['radioma2'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radioma2" value="1"<?php if ($teste['radioma2'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radioma2" hidden>
                                                        <select class="form-control input-sm" id="especifique_radioma2_val">
                                                            <?php if (isset($teste['especifique_radioma2_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radioma2_val']) ?>"><?= ($teste['especifique_radioma2_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Lateralidade D</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radioma3" value="3"<?php if ($teste['radioma3'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radioma3" value="2"<?php if ($teste['radioma3'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radioma3" value="1" <?php if ($teste['radioma3'] == 1) echo 'checked'; ?>class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radioma3" hidden>
                                                        <select class="form-control input-sm" id="especifique_radioma3_val">
                                                            <?php if (isset($teste['especifique_radioma3_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radioma3_val']) ?>"><?= ($teste['especifique_radioma3_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Lateralidade E</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radioma4" value="3"<?php if ($teste['radioma4'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radioma4" value="2"<?php if ($teste['radioma4'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radioma4" value="1"<?php if ($teste['radioma4'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radioma4" hidden>
                                                        <select class="form-control input-sm" id="especifique_radioma4_val">
                                                            <?php if (isset($teste['especifique_radioma4_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radioma4_val']) ?>"><?= ($teste['especifique_radioma4_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>   
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Movimentos da língua <label style="float: right; font-size: 15pt" id="tab2_2_result" class="result_tabs">
                                                <?php if (isset($teste['tab2_2_result'])): ?>
                                                    <?= ($teste['tab2_2_result']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>  
                                            </label></h4>
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            Desempenho
                                                        </label>
                                                        <label>
                                                            Preciso
                                                        </label>
                                                        <label>
                                                            Falta de precisão/tremor
                                                        </label>
                                                        <label>
                                                            Inabilidade severa
                                                        </label>
                                                        <div id="especifique_radiomb" hidden>
                                                            <label>
                                                                Especifique:
                                                            </label></div>   
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 11%;">
                                                <label>Protruir</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomb1" value="3"<?php if ($teste['radiomb1'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb1" value="2"<?php if ($teste['radiomb1'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb1" value="1"<?php if ($teste['radiomb1'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomb1" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomb1_val">
                                                            <?php if (isset($teste['especifique_radiomb1_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomb1_val']) ?>"><?= ($teste['especifique_radiomb1_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>   
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 11%;">
                                                <label>Retrair</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomb2" value="3"<?php if ($teste['radiomb2'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb2" value="2"<?php if ($teste['radiomb2'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb2" value="1"<?php if ($teste['radiomb2'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomb2" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomb2_val">
                                                            <?php if (isset($teste['especifique_radiomb2_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomb2_val']) ?>"><?= ($teste['especifique_radiomb2_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 11%;">
                                                <label>Lateral D</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomb3" value="3"<?php if ($teste['radiomb3'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb3" value="2"<?php if ($teste['radiomb3'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb3" value="1"<?php if ($teste['radiomb3'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomb3" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomb3_val">
                                                            <?php if (isset($teste['especifique_radiomb3_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomb3_val']) ?>"><?= ($teste['especifique_radiomb3_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 11%;">
                                                <label>Lateral E</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomb4"<?php if ($teste['radiomb4'] == 3) echo 'checked'; ?> value="3" class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb4" value="2"<?php if ($teste['radiomb4'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb4" value="1"<?php if ($teste['radiomb4'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomb4" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomb4_val">
                                                            <?php if (isset($teste['especifique_radiomb4_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomb4_val']) ?>"><?= ($teste['especifique_radiomb4_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 11%;">
                                                <label>Elevar</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomb5" value="3"<?php if ($teste['radiomb5'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb5" value="2"<?php if ($teste['radiomb5'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb5" value="1"<?php if ($teste['radiomb5'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomb5" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomb5_val">
                                                            <?php if (isset($teste['especifique_radiomb5_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomb5_val']) ?>"><?= ($teste['especifique_radiomb5_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 11%;">
                                                <label>Abaixar</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomb6"<?php if ($teste['radiomb6'] == 3) echo 'checked'; ?> value="3" class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb6" value="2"<?php if ($teste['radiomb6'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomb6" value="1"<?php if ($teste['radiomb6'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomb6" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomb6_val">
                                                            <?php if (isset($teste['especifique_radiomb6_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomb6_val']) ?>"><?= ($teste['especifique_radiomb6_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Movimentos da mandíbula <label style="float: right; font-size: 15pt" id="tab2_3_result" class="result_tabs">
                                                <?php if (isset($teste['tab2_3_result'])): ?>
                                                    <?= ($teste['tab2_3_result']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>  
                                            </label></h4>
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            Desempenho
                                                        </label>
                                                        <label>
                                                            Preciso
                                                        </label>
                                                        <label>
                                                            Falta de precisão/Desvio
                                                        </label>
                                                        <label>
                                                            Inabilidade severa
                                                        </label>
                                                        <div id="desvio_radiomc" hidden>
                                                            <label>
                                                                Desvio para o lado
                                                            </label></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 13%;">
                                                <label>Abaixar</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomc1" value="3"<?php if ($teste['radiomc1'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc1" value="2"<?php if ($teste['radiomc1'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc1" value="1"<?php if ($teste['radiomc1'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                        <div id="desvio_radiomc1" hidden>
                                                            <select class="form-control input-sm" id="desvio_radiomc1_val">
                                                                <?php if (isset($teste['desvio_radiomc1_val'])): ?>
                                                                    <option value="<?= ($teste['desvio_radiomc1_val']) ?>"><?= ($teste['desvio_radiomc1_val']) ?></option>
                                                                    <?php
                                                                else:
                                                                    ?>
                                                                    <option value="">-</option>
                                                                    <option value="direita">D</option>
                                                                    <option value="esquerda">E</option>
                                                                <?php
                                                                endif;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 13%;">
                                                <label>Elevar</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomc2" value="3"<?php if ($teste['radiomc2'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc2" value="2"<?php if ($teste['radiomc2'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc2" value="1"<?php if ($teste['radiomc2'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                        <div id="desvio_radiomc2" hidden>
                                                            <select class="form-control input-sm" id="desvio_radiomc2_val">
                                                                <?php if (isset($teste['desvio_radiomc2'])): ?>
                                                                    <option value="<?= ($teste['desvio_radiomc2']) ?>"><?= ($teste['desvio_radiomc2']) ?></option>
                                                                    <?php
                                                                else:
                                                                    ?>
                                                                    <option value="">-</option>
                                                                    <option value="direita">D</option>
                                                                    <option value="esquerda">E</option>
                                                                <?php
                                                                endif;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 13%;">
                                                <label>Lateral D</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomc4"<?php if ($teste['radiomc4'] == 3) echo 'checked'; ?> value="3" class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc4" value="2"<?php if ($teste['radiomc4'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc4" value="1"<?php if ($teste['radiomc4'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 13%;">
                                                <label>Lateral E</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomc5"<?php if ($teste['radiomc5'] == 3) echo 'checked'; ?> value="3" class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc5" value="2"<?php if ($teste['radiomc5'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc5" value="1"<?php if ($teste['radiomc5'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="width: 13%;">
                                                <label>Protruir</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomc3"<?php if ($teste['radiomc3'] == 3) echo 'checked'; ?> value="3" class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc3"<?php if ($teste['radiomc3'] == 2) echo 'checked'; ?> value="2" class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomc3"<?php if ($teste['radiomc3'] == 1) echo 'checked'; ?> value="1" class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="desvio_radiomc3"hidden>
                                                        <select class="form-control input-sm" id="desvio_radiomc3_val">
                                                            <?php if (isset($teste['desvio_radiomc3_val'])): ?>
                                                                <option value="<?= ($teste['desvio_radiomc3_val']) ?>"><?= ($teste['desvio_radiomc3_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="direita">D</option>
                                                                <option value="esquerda">E</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Movimentos de bochecha<label style="float: right; font-size: 15pt" id="tab2_4_result" class="result_tabs">
                                                <?php if (isset($teste['tab2_4_result'])): ?>
                                                    <?= ($teste['tab2_4_result']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>    
                                            </label></h4>
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            Desempenho
                                                        </label>
                                                        <label>
                                                            Preciso
                                                        </label>
                                                        <label>
                                                            Falta de precisão/tremor
                                                        </label>
                                                        <label>
                                                            Inabilidade severa
                                                        </label>
                                                        <div id="especifique_radiomd" hidden>
                                                            <label>
                                                                Especifique:
                                                            </label></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Inflar</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomd1" value="3"<?php if ($teste['radiomd1'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomd1" value="2"<?php if ($teste['radiomd1'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomd1" value="1"<?php if ($teste['radiomd1'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomd1" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomd1_val">
                                                            <?php if (isset($teste['especifique_radiomd1_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomd1_val']) ?>"><?= ($teste['especifique_radiomd1_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Sugar</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomd2" value="3"<?php if ($teste['radiomd2'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomd2" value="2"<?php if ($teste['radiomd2'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomd2" value="1"<?php if ($teste['radiomd2'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomd2" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomd2_val">
                                                            <?php if (isset($teste['especifique_radiomd2_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomd2_val']) ?>"><?= ($teste['especifique_radiomd2_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Retrair</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomd3"<?php if ($teste['radiomd3'] == 3) echo 'checked'; ?> value="3" class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomd3" value="2"<?php if ($teste['radiomd3'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomd3" value="1"<?php if ($teste['radiomd3'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomd3" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomd3_val">
                                                            <?php if (isset($teste['especifique_radiomd3_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomd3_val']) ?>"><?= ($teste['especifique_radiomd3_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Lateralizar o ar</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radiomd4"<?php if ($teste['radiomd4'] == 3) echo 'checked'; ?> value="3" class="icheck" data-radio="iradio_flat-blue"> (3) </label>
                                                        <label>
                                                            <input type="radio" name="radiomd4" value="2"<?php if ($teste['radiomd4'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) </label>
                                                        <label>
                                                            <input type="radio" name="radiomd4" value="1"<?php if ($teste['radiomd4'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) </label>
                                                    </div>
                                                    <div id="especifique_radiomd4" hidden>
                                                        <select class="form-control input-sm" id="especifique_radiomd4_val">
                                                            <?php if (isset($teste['especifique_radiomd4_val'])): ?>
                                                                <option value="<?= ($teste['especifique_radiomd4_val']) ?>"><?= ($teste['especifique_radiomd4_val']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="">-</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Falta de precisão">Falta de precisão</option>
                                                                <option value="Tremor">Tremor</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Observações - Mobilidade</h4>
                                        <div class='form-group'>
                                            <div class="input-icon input-icon-lg">
                                                <div class='col-md-12'>
                                                    <textarea class='form-control' rows='3' name="observacoes_mobilidade" value="<?= $teste['observacoes_mobilidade'] ?>"  class="icheck" data-radio="iradio_flat-blue"><?= $teste['observacoes_mobilidade'] ?> </textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <br>
                                        <h4 class="form-section"><b>Total Mobilidade</b>
                                            <label id="tab2_result" class="result_tabs" style="float: right; font-size: 15pt">
                                                <?php if (isset($teste['tab2_result'])): ?>
                                                    <?= ($teste['tab2_result']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>
                                            </label></h4>
                                        <h4 class="form-section"><b>Total geral</b> 
                                            <label class="tab1_resultgeral" style="float: right; font-size: 15pt">
                                                <?php if (isset($teste['tab1_resultgeral'])): ?>
                                                    <?= ($teste['tab1_resultgeral']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>
                                            </label></h4>
                                        <div class="form-section">
                                            <a href="#tab4"  onclick="muda_botao('bntetp4')" data-toggle="tab" class="btn blue"style="float:right;">
                                                Seguinte</a>
                                            <a href="#tab2"  onclick="muda_botao('bntetp2')" data-toggle="tab" class="btn blue"style="float:left;">
                                                Voltar
                                            </a>
                                        </div>
                                    </form>
                                </div>
                                <!--Etapa 4 - Funções -->
                                <div class="tab-pane" id="tab4">
                                    <form role='form' id='funcoes'>
                                        <h3 class="block"><b>Funções</b></h3>
                                        <h4 class="form-section"><b>Respiração <label id="tab3_1_result" class="result_tabs" style="float: right; font-size: 15pt">
                                                    <?php if (isset($teste['tab3_1_result'])): ?>
                                                        <?= ($teste['tab3_1_result']) ?>
                                                    <?php else:
                                                        ?>0<?php endif; ?>
                                                </label></b></h4>
                                        <div id='respiracao'>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Respiração nasal
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof1" value="3" <?php if ($teste['radiof1'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Normal
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Respiração oronasal
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof1" value="2" <?php if ($teste['radiof1'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Leve
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof1" value="1" <?php if ($teste['radiof1'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Severa
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section"><b>Deglutição <label id="tab3_2_result" class="result_tabs" style="float: right; font-size: 15pt">
                                                    <?php if (isset($teste['tab3_2_result'])): ?>
                                                        <?= ($teste['tab3_2_result']) ?>
                                                    <?php else:
                                                        ?>0<?php endif; ?>
                                                </label></b></h4>
                                        <h4 class="form-section">Deglutição: Comportamento dos lábios</h4>
                                        <div id='degluticao'>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Oclusão normal dos lábios
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof2" value="4" <?php if ($teste['radiof2'] == 4) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (4) Sem aparentar esforço
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Oclusão dos lábios com esforço
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof2" value="3" <?php if ($teste['radiof2'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Leve
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof2" value="2" <?php if ($teste['radiof2'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Moderada
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Não vedam a cavidade oral
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof2" value="1" <?php if ($teste['radiof2'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Severa
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="form-section">Deglutição: Comportamento da língua</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Contida na cavidade oral
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof3" value="3" <?php if ($teste['radiof3'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Normal
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Interposta aos arcos dentários
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof3" value="2" <?php if ($teste['radiof3'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Adaptação ou disfunção
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof3" value="1" <?php if ($teste['radiof3'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Protruída em excesso
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group"hidden id="radiof3_local">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Local
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof3_local" value="anterior"<?php if ($teste['radiof3_local'] == 'anterior') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Anterior
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof3_local" value="lateral_posterior"<?php if ($teste['radiof3_local'] == 'lateral_posterior') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Lateral/Posterior
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof3_local" value="toda_extensao"<?php if ($teste['radiof3_local'] == 'toda_extensao') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Toda a extensão do arco
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <h4 class="form-section">Deglutição: Outros comportamentos e sinais de alteração</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Movimentação da cabeça
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof4" value="1" <?php if ($teste['radiof4'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Ausente
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof4" value="0" <?php if ($teste['radiof4'] === 0) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (0) Presente
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Tensão dos músculos faciais
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof5" value="1" <?php if ($teste['radiof5'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Ausente
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof5" value="0" <?php if ($teste['radiof5'] === 0) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (0) Presente
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Escape de alimento
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof6" value="1" <?php if ($teste['radiof6'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Ausente
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof6" value="0" <?php if ($teste['radiof6'] === 0) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (0) Presente
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="form-section">Deglutição: Eficiência Item complementar</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Bolo sólido
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof7" value="3" <?php if ($teste['radiof7'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Não repete a deglutição do mesmo bolo
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof7" value="2" <?php if ($teste['radiof7'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Uma repetição
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof7" value="1" <?php if ($teste['radiof7'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Deglutições múltiplas
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Bolo líquido
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof8" value="3" <?php if ($teste['radiof8'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Não repete a deglutição do mesmo bolo
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof8" value="2" <?php if ($teste['radiof8'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Uma repetição
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof8" value="1" <?php if ($teste['radiof8'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Deglutições múltiplas
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <h4 class="form-section"><b>Mastigação <label id="tab3_3_result" class="result_tabs" style="float: right; font-size: 15pt">
                                                    <?php if (isset($teste['tab3_3_result'])): ?>
                                                        <?= ($teste['tab3_3_result']) ?>
                                                    <?php else:
                                                        ?>0<?php endif; ?>
                                                </label></b></h4>
                                        <div id="mastigacao">
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Morde com dentes incisivos
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof9" value="3" <?php if ($teste['radiof9'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Morde com dentes posteriores
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof9" value="2" <?php if ($teste['radiof9'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Não morde
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof9" value="1" <?php if ($teste['radiof9'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h4 class="form-section"></h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Bilateral
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof10" value="4" <?php if ($teste['radiof10'] == 4) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (4) Alternada
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof10" value="3" <?php if ($teste['radiof10'] == 3) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (3) Simultânea (vertical)
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Unilateral
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" id="radiof10_pr" name="radiof10" value="2" <?php if ($teste['radiof10'] == 2) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (2) Preferencial (66% do mesmo lado)
                                                            </label>
                                                            <label>
                                                                <input type="radio"id="radiof10_cr" name="radiof10" value="1" <?php if ($teste['radiof10'] == 1 && $teste['radiof10_radiovalue'] == 'cronica') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Crônica (95% do mesmo lado)
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group" hidden id="radiof10_lado">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Lado (%)
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof10_lado" value="direito"<?php if (($teste['radiof10'] == 2 || $teste['radiof10'] == 1) && $teste['radiof10_lado'] == 'direito') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Direito
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof10_lado" value="esquerdo"<?php if (($teste['radiof10'] == 2 || $teste['radiof10'] == 1) && $teste['radiof10_lado'] == 'esquerdo') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> Esquerdo
                                                            </label>
                                                            <label>
                                                                <div class="input-icon input-icon-sm right">
                                                                    <i>%</i>
                                                                    <input id="radiof10_lado_percent" value="<?= ($teste['radiof10_lado_percent']) ?>" type="number" min="0" max="100" step=".01" class="form-control input-sm" placeholder="%" style=" width: 107px;">
                                                                </div>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Anterior (Frontal)
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio"id="radiof10_af" name="radiof10" value="1" <?php if ($teste['radiof10'] == 1 && $teste['radiof10_radiovalue'] == 'anterior_frontal') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) 
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Não realiza a função
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" id="radiof10_nt" name="radiof10" value="1" <?php if ($teste['radiof10'] == 1 && $teste['radiof10_radiovalue'] == 'nao_tritura') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Não tritura
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h4 class="form-section">Outros comportamentos e sinais de alteração</h4>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Movimentação da cabeça ou outras partes do corpo
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof11" value="1" <?php if ($teste['radiof11'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Ausente
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof11" value="0" <?php if ($teste['radiof11'] === 0) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (0) Presente
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Postura alterada
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof12" value="1" <?php if ($teste['radiof12'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Ausente
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof12" value="0" <?php if ($teste['radiof12'] === 0) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (0) Presente
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-6" style="padding-top: 0px" >
                                                    Escape de alimento
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <div class="icheck-list">
                                                            <label>
                                                                <input type="radio" name="radiof13" value="1" <?php if ($teste['radiof13'] == 1) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (1) Ausente
                                                            </label>
                                                            <label>
                                                                <input type="radio" name="radiof13" value="0" <?php if ($teste['radiof13'] === 0) echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> (0) Presente
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-8">Alimento utilizado
                                                    <input type="text" class='form-control' id="food" value="<?= $teste['food'] ?>">
                                                </div>
                                                <div class="col-md-4">Tempo gasto para ingerir o alimento
                                                    <div class="input-icon">
                                                        <i class="input-icon input-icon-sm right" style=" left: 230px;">seg</i>
                                                        <i class="fa fa-clock-o"></i>
                                                        <input type="text" class='form-control' id="spent_time_food" value="<?= $teste['spent_time_food'] ?>">
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                        <h4 class="form-section">Observações - Funções</h4>
                                        <div class='form-group'>
                                            <div class="input-icon input-icon-lg">
                                                <div class='col-md-12'>
                                                    <textarea class='form-control' rows='3' name="observacoes_funcoes" value="<?= $teste['observacoes_funcoes'] ?>"  class="icheck" data-radio="iradio_flat-blue"><?= $teste['observacoes_funcoes'] ?> </textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <h4 class="form-section"><b>Total Funções</b> 
                                            <label id="tab3_result" class="result_tabs" style="float: right; font-size: 15pt">
                                                <?php if (isset($teste['tab3_result'])): ?>
                                                    <?= ($teste['tab3_result']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>
                                            </label></h4>                                      
                                        <h4 class="form-section"><b>Total geral</b> 
                                            <label class="tab1_resultgeral" style="float: right; font-size: 15pt">
                                                <?php if (isset($teste['tab1_resultgeral'])): ?>
                                                    <?= ($teste['tab1_resultgeral']) ?>
                                                <?php else:
                                                    ?>0<?php endif; ?>
                                            </label></h4>
                                        <div class="form-section">
                                            <a href="#tab5"  onclick="muda_botao('bntetp5')" data-toggle="tab" class="btn blue"style="float:right;">
                                                Seguinte                            
                                            </a>
                                            <a href="#tab3"  onclick="muda_botao('bntetp3')" data-toggle="tab" class="btn blue"style="float:left;">
                                                Voltar
                                            </a>
                                        </div>

                                    </form>
                                </div>
                                <!--Etapa 5 - Análise da oclusão -->
                                <div class="tab-pane" id="tab5">
                                    <form role='form' id='analise'>
                                        <h3 class="block"><b>Análise da oclusão</b></h3>
                                        <h4 class="form-section">Classificação de Angle </h4>
                                        <div class="form-group">
                                            <div class="col-md-2"style="top: 28px;">
                                                <div class="input-group">
                                                    <div class="icheck-list">

                                                        <label>
                                                            Lado Direito
                                                        </label>
                                                        <label>
                                                            Lado Esquerdo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Classe I</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radioaod" value="classe 1"<?php if ($teste['radioaod'] == 'classe 1') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                        <label>
                                                            <input type="radio" name="radioaoe" value="classe 1"<?php if ($teste['radioaoe'] == 'classe 1') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Classe II - div 1</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radioaod" value="classe 2 - div 1"<?php if ($teste['radioaod'] == 'classe 2 - div 1') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                        <label>
                                                            <input type="radio" name="radioaoe" value="classe 2 - div 1"<?php if ($teste['radioaoe'] == 'classe 2 - div 1') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Classe II - div 2</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radioaod" value="classe 2 - div 2"<?php if ($teste['radioaod'] == 'classe 2 - div 2') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                        <label>
                                                            <input type="radio" name="radioaoe" value="classe 2 - div 2"<?php if ($teste['radioaoe'] == 'classe 2 - div 2') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Classe III</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radioaod" value="classe 3"<?php if ($teste['radioaod'] == 'classe 3') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                        <label>
                                                            <input type="radio" name="radioaoe" value="classe 3"<?php if ($teste['radioaoe'] == 'classe 3') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label>Não classifica</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <input type="radio" name="radioaod" value="Não classifica"<?php if ($teste['radioaod'] == 'Não classifica') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                        <label>
                                                            <input type="radio" name="radioaoe" value="Não classifica"<?php if ($teste['radioaoe'] == 'Não classifica') echo 'checked'; ?> class="icheck" data-radio="iradio_flat-blue"> </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>       
                                        <h3 class="block"><b>Análise funcional da oclusão</b></h3>
                                        <h4 class="form-section">Linha média </h4>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="icheck-inline">
                                                    <div class="col-md-4">
                                                        <label>
                                                            <div class="icheckbox_flat-blue" style="position: relative;">
                                                                <input id="linha_media_normal" <?php if ($teste['linha_media_normal']) echo 'checked'; ?> type="checkbox" class="icheck" data-checkbox="icheckbox_flat-blue" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
                                                            </div>
                                                            Normal
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>
                                                            <div class="icheckbox_flat-blue" style="position: relative;">
                                                                <input id="linha_media_direita"<?php if ($teste['linha_media_direita']) echo 'checked'; ?> type="checkbox" class="icheck" data-checkbox="icheckbox_flat-blue" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
                                                            </div>
                                                            Desviada para direita
                                                        </label>
                                                        <label class="linha_media_direita_mm" hidden="">
                                                            <div class="input-icon input-icon-sm right">
                                                                <i>mm</i>
                                                                <input id="linha_media_direita_mm" value="<?= ($teste['linha_media_direita_mm']) ?>" type="number" min="0" max="100" step=".01"  class="form-control input-sm">
                                                            </div>
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>
                                                            <div class="icheckbox_flat-blue" style="position: relative;">
                                                                <input id="linha_media_esquerda" <?php if ($teste['linha_media_esquerda']) echo 'checked'; ?>  type="checkbox" class="icheck" data-checkbox="icheckbox_flat-blue" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
                                                            </div>
                                                            Desviada para esquerda
                                                        </label>
                                                        <label class="linha_media_esquerda_mm" value="<?= ($teste['linha_media_esquerda_mm']) ?>" hidden="">
                                                            <div class="input-icon input-icon-sm right">
                                                                <i>mm</i>
                                                                <input id="linha_media_esquerda_mm" type="number" step=".01" min="0" max="100" class="form-control input-sm">
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="block">Movimentos Mandibulares</h3>
                                        <h4 class="form-section">Movimentos</h4>
                                        <div class="form-group">
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <br>
                                                        </label>
                                                        <label>
                                                            Abertura
                                                        </label>
                                                        <br>
                                                        <label>
                                                            Fechamento
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1" style="text-align: center;">
                                                <label>Normal</label>
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <div class="icheckbox_flat-blue" style="position: relative;">
                                                                <input id="mov_abert_normal"<?php if ($teste['mov_abert_normal']) echo 'checked'; ?> type="checkbox" class="icheck" data-checkbox="icheckbox_flat-blue" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
                                                            </div>
                                                        </label>
                                                        <br>
                                                        <label>
                                                            <div class="icheckbox_flat-blue" style="position: relative;">
                                                                <input id="mov_fecham_normal"<?php if ($teste['mov_fecham_normal']) echo 'checked'; ?> type="checkbox" class="icheck" data-checkbox="icheckbox_flat-blue" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; border: 0px; opacity: 0; background: rgb(255, 255, 255);"></ins>
                                                            </div>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="col-md-3">
                                                <label style="padding-left: 50px">Desvio</label>
                                                <div class="input-group">
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="aber_desvio" name="aber_desvio">
                                                            <?php if (isset($teste['aber_desvio'])): ?>
                                                                <option value="<?= ($teste['aber_desvio']) ?>"><?= ($teste['aber_desvio']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Direita">Direita</option>
                                                                <option value="Esquerda">Esquerda</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                    <br>
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="fech_desvio" name="fech_desvio">
                                                            <?php if (isset($teste['fech_desvio'])): ?>
                                                                <option value="<?= ($teste['fech_desvio']) ?>"><?= ($teste['fech_desvio']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Direita">Direita</option>
                                                                <option value="Esquerda">Esquerda</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <label style="padding-left: 50px">Dor</label>
                                                <div class="input-group">
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="aber_dor" name="aber_dor">
                                                            <?php if (isset($teste['aber_dor'])): ?>
                                                                <option value="<?= ($teste['aber_dor']) ?>"><?= ($teste['aber_dor']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Não">Não</option>

                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Direita">Direita</option>
                                                                <option value="Esquerda">Esquerda</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                    <br>
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="fech_dor" name="fech_dor">
                                                            <?php if (isset($teste['fech_dor'])): ?>
                                                                <option value="<?= ($teste['fech_dor']) ?>"><?= ($teste['fech_dor']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Não">Não</option>

                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Direita">Direita</option>
                                                                <option value="Esquerda">Esquerda</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <!-- Trepasse -->
                                                <div class="col-md-2">
                                                    <label>Trespasse Vertical</label>
                                                    <div class="input-icon input-icon-sm right">
                                                        <i>mm</i>
                                                        <input id="trespasse_vertical" value="<?= ($teste['trespasse_vertical']) ?>"type="number" min="0" max="99" step=".01" class="form-control input-sm">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <label> Distância Interincisal</label>
                                                    <div class="input-icon input-icon-sm right">
                                                        <i>mm</i>
                                                        <input id="dist_inter_incisivos" value="<?= ($teste['trespasse_vertical']) ?>" type="number" min="0" max="99" step=".01" class="form-control input-sm">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <label>Total</label>
                                                    <div class="input-icon input-icon-sm right">
                                                        <i>mm</i>
                                                        <input id="total_trespasse_incisivos" value="<?= ($teste['total_trespasse_incisivos']) ?>" type="text" class="form-control input-sm" step=".01" readonly="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                        <h4 class="form-section">Lateralidade</h4>
                                        <div class="form-group">
                                            <div class="col-md-1">
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <br>
                                                        </label>
                                                        <label>
                                                            <br>
                                                        </label>
                                                        <label>
                                                            Direita
                                                        </label>
                                                        <label style=" padding-top: 15px;">
                                                            Esquerda
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2"style="text-align: center;">
                                                <label>Dor</label>
                                                <div class="form-group"style=" padding-top: 4px; ">
                                                    <div class="col-md-8">
                                                        <label></label>
                                                        <select id="lat_dir_dor" class="layout-style-option form-control input-small"name="lat_dir_dor">
                                                            <?php if (isset($teste['lat_dir_dor'])): ?>
                                                                <option value="<?= ($teste['lat_dir_dor']) ?>"><?= ($teste['lat_dir_dor']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Não">Não</option>

                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Direita">Direita</option>
                                                                <option value="Esquerda">Esquerda</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <select id="lat_esq_dor" class="layout-style-option form-control input-small">
                                                            <?php if (isset($teste['lat_esq_dor'])): ?>
                                                                <option value="<?= ($teste['lat_esq_dor']) ?>"><?= ($teste['lat_esq_dor']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Não">Não</option>
                                                                <option value="Ambos">Ambos</option>
                                                                <option value="Direita">Direita</option>
                                                                <option value="Esquerda">Esquerda</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2"style="text-align: center;">
                                                <label>Guias oclusal</label>
                                                <div class="form-group" style=" padding-top: 4px;">
                                                    <div class="col-md-8">
                                                        <label></label>
                                                        <select id="lat_guia_dir" class="layout-style-option form-control input-small">
                                                            <?php if (isset($teste['lat_guia_dir'])): ?>
                                                                <option value="<?= ($teste['lat_guia_dir']) ?>"><?= ($teste['lat_guia_dir']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Canina">Canina</option>
                                                                <option value="Incisal">Incisal</option>
                                                                <option value="Função em Grupo">Função em Grupo</option>
                                                                <option value="Ausente">Ausente</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <select id="lat_guia_esq" class="layout-style-option form-control input-small">
                                                            <?php if (isset($teste['lat_guia_esq'])): ?>
                                                                <option value="<?= ($teste['lat_guia_esq']) ?>"><?= ($teste['lat_guia_esq']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Canina">Canina</option>
                                                                <option value="Incisal Central">Incisal Central</option>
                                                                <option value="Incisal Lateral">Incisal Lateral</option>
                                                                <option value="Função em Grupo">Função em Grupo</option>
                                                                <option value="Ausente">Ausente</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4" style="text-align: center;">
                                                <label>Interferência oclusal</label>
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>Trabalho</label>
                                                        <select class="layout-style-option form-control input-small" id="lat_int_trab_dir">
                                                            <?php if (isset($teste['lat_int_trab_dir'])): ?>
                                                                <option value="<?= ($teste['lat_int_trab_dir']) ?>"><?= ($teste['lat_int_trab_dir']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                        <select class="layout-style-option form-control input-small" id="lat_int_trab_esq">
                                                            <?php if (isset($teste['lat_int_trab_esq'])): ?>
                                                                <option value="<?= ($teste['lat_int_trab_esq']) ?>"><?= ($teste['lat_int_trab_esq']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select> 
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Balanceio</label>
                                                        <select class="layout-style-option form-control input-small" id="lat_int_bal_dir">
                                                            <?php if (isset($teste['lat_int_bal_dir'])): ?>
                                                                <option value="<?= ($teste['lat_int_bal_dir']) ?>"><?= ($teste['lat_int_bal_dir']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select> 
                                                        <select class="layout-style-option form-control input-small" id="lat_int_bal_esq">
                                                            <?php if (isset($teste['lat_int_bal_esq'])): ?>
                                                                <option value="<?= ($teste['lat_int_bal_esq']) ?>"><?= ($teste['lat_int_bal_esq']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2"style="text-align: center;left: 23px;top: 28px;">
                                                <label>Medidas</label>
                                                <div class='form-group'>
                                                    <div class="input-icon input-icon-sm right ">
                                                        <i>mm</i>
                                                        <input id="lat_medidas_dir"value="<?= $teste['lat_medidas_dir'] ?>" type="number" min="0" max="100" step=".01" class="form-control input-sm" >
                                                    </div>
                                                    <div class="input-icon input-icon-sm right">
                                                        <i>mm</i>
                                                        <input id="lat_medidas_esq" value="<?= $teste['lat_medidas_esq'] ?>" type="number" min="0" max="100" step=".01" class="form-control input-sm" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h3 class="block">Protrusão</h3>
                                        <div class="form-group">
                                            <div class="col-md-4" style="text-align: center;">
                                                <label>Movimento</label>
                                                <div class="form-group">
                                                    <div class="col-md-6">
                                                        <label>Dor</label>
                                                        <div class="input-group">
                                                            <label>
                                                                <select class="layout-style-option form-control input-small" id="pro_mov_dor" name="pro_mov_dor">
                                                                    <?php if (isset($teste['pro_mov_dor'])): ?>
                                                                        <option value="<?= ($teste['pro_mov_dor']) ?>"><?= ($teste['pro_mov_dor']) ?></option>
                                                                        <?php
                                                                    else:
                                                                        ?>
                                                                        <option value="" selected="selected"></option>
                                                                        <option value="Não">Não</option>
                                                                        <option value="Ambos">Ambos</option>
                                                                        <option value="Direita">Direita</option>
                                                                        <option value="Esquerda">Esquerda</option>
                                                                    <?php
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Desvio</label>
                                                        <div class="input-group">
                                                            <label>
                                                                <select class="layout-style-option form-control input-small" id="pro_mov_desv" name="pro_mov_desv">
                                                                    <?php if (isset($teste['pro_mov_desv'])): ?>
                                                                        <option value="<?= ($teste['pro_mov_desv']) ?>"><?= ($teste['pro_mov_desv']) ?></option>
                                                                        <?php
                                                                    else:
                                                                        ?>
                                                                        <option value="" selected="selected"></option>
                                                                        <option value="Direita">Direita</option>
                                                                        <option value="Esquerda">Esquerda</option>
                                                                    <?php
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2" style="text-align: center;">
                                                <label></label>
                                                <div class="form-group">

                                                    <div class="col-md-12">
                                                        <label>Interferência Oclusal</label>
                                                        <div class="input-group">
                                                            <label>
                                                                <select class="layout-style-option form-control input-small" id="pro_interf" name="pro_interf">
                                                                    <?php if (isset($teste['pro_interf'])): ?>
                                                                        <option value="<?= ($teste['pro_interf']) ?>"><?= ($teste['pro_interf']) ?></option>
                                                                        <?php
                                                                    else:
                                                                        ?>
                                                                        <option value="" selected="selected"></option>
                                                                        <option value="Não">Não</option>
                                                                        <option value="Ambos">Ambos</option>
                                                                        <option value="Direita">Direita</option>
                                                                        <option value="Esquerda">Esquerda</option>
                                                                    <?php
                                                                    endif;
                                                                    ?>
                                                                </select>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-md-6" style="text-align: center;">
                                                <label>Medidas</label>
                                                <div class="form-group">
                                                    <div class="col-md-4">
                                                        <label>Trespasse horizontal</label>
                                                        <div class="input-icon input-icon-sm right">
                                                            <i>mm</i>
                                                            <input id="pro_med_tresp" value="<?= $teste['pro_med_tresp'] ?>" type="number" min="0" maxlength="100" step=".01" class="form-control input-sm">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Distância</label>
                                                        <div class="input-icon input-icon-sm right">
                                                            <i>mm</i>
                                                            <input id="pro_med_dist" value="<?= $teste['pro_med_dist'] ?>" type="number" min="0" maxlength="100" step=".01" class="form-control input-sm">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <label>Total</label>
                                                        <div class="input-icon input-icon-sm right">
                                                            <i>mm</i>
                                                            <input id="pro_med_total" value="<?= $teste['pro_med_total'] ?>"min="0" maxlength="100" type="number "step=".01" class="form-control input-sm" readonly="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Ruido na ATM</h4>
                                        <div class="form-group">
                                            <div class="col-md-2">
                                                <div class="input-group">
                                                    <div class="icheck-list">
                                                        <label>
                                                            <br>
                                                        </label>
                                                        <label>
                                                            Lado Direito
                                                        </label>
                                                        <br>
                                                        <label>
                                                            Lado Esquerdo
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2"style="text-align: center;">
                                                <label>Abertura</label>
                                                <div class="input-group">
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_abert_dir">
                                                            <?php if (isset($teste['atm_abert_dir'])): ?>
                                                                <option value="<?= ($teste['atm_abert_dir']) ?>"><?= ($teste['atm_abert_dir']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                    <br>
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_abert_esq">
                                                            <?php if (isset($teste['atm_abert_esq'])): ?>
                                                                <option value="<?= ($teste['atm_abert_esq']) ?>"><?= ($teste['atm_abert_esq']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-2"style="text-align: center;">
                                                <label>Fechamento</label>
                                                <div class="input-group">
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_fech_dir">
                                                            <?php if (isset($teste['atm_fech_dir'])): ?>
                                                                <option value="<?= ($teste['atm_fech_dir']) ?>"><?= ($teste['atm_fech_dir']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>
                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                    <br>
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_fech_esq">
                                                            <?php if (isset($teste['atm_fech_esq'])): ?>
                                                                <option value="<?= ($teste['atm_fech_esq']) ?>"><?= ($teste['atm_fech_esq']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-2"style="text-align: center;">
                                                <label>Protrusão</label>
                                                <div class="input-group">
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_prot_dir">
                                                            <?php if (isset($teste['atm_prot_dir'])): ?>
                                                                <option value="<?= ($teste['atm_prot_dir']) ?>"><?= ($teste['atm_prot_dir']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                    <br>
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_prot_esq">
                                                            <?php if (isset($teste['atm_prot_esq'])): ?>
                                                                <option value="<?= ($teste['atm_prot_esq']) ?>"><?= ($teste['atm_prot_esq']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-2"style="text-align: center;">
                                                <label>Lateral Direita</label>
                                                <div class="input-group">
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_lat_dir_dir">
                                                            <?php if (isset($teste['atm_lat_dir_dir'])): ?>
                                                                <option value="<?= ($teste['atm_lat_dir_dir']) ?>"><?= ($teste['atm_lat_dir_dir']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                    <br>
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_lat_dir_esq">
                                                            <?php if (isset($teste['atm_lat_dir_esq'])): ?>
                                                                <option value="<?= ($teste['atm_lat_dir_esq']) ?>"><?= ($teste['atm_lat_dir_esq']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-2"style="text-align: center;">
                                                <label>Lateral Esquerda</label>
                                                <div class="input-group">
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_lat_esq_dir">
                                                            <?php if (isset($teste['atm_lat_esq_dir'])): ?>
                                                                <option value="<?= ($teste['atm_lat_esq_dir']) ?>"><?= ($teste['atm_lat_esq_dir']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                    <br>
                                                    <label>
                                                        <select class="layout-style-option form-control input-small" id="atm_lat_esq_esq">
                                                            <?php if (isset($teste['atm_lat_esq_esq'])): ?>
                                                                <option value="<?= ($teste['atm_lat_esq_esq']) ?>"><?= ($teste['atm_lat_esq_esq']) ?></option>
                                                                <?php
                                                            else:
                                                                ?>
                                                                <option value="" selected="selected"></option>
                                                                <option value="Sim">Sim</option>
                                                                <option value="Não">Não</option>                                                            <?php
                                                            endif;
                                                            ?>
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <h4 class="form-section">Observações - Análise da Oclusão</h4>
                                        <div class='form-group'>
                                            <div class="input-icon input-icon-lg">
                                                <div class='col-md-12'>
                                                    <textarea class='form-control' rows='3' name="observacoes_analise" value="<?= $teste['observacoes_analise'] ?>"  class="icheck" data-radio="iradio_flat-blue"><?= $teste['observacoes_analise'] ?> </textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-section">
                                            <h4 class="form-section"><b>Total geral</b> 
                                                <label class="tab1_resultgeral"  style="float: right; font-size: 15pt">
                                                    <?php if (isset($teste['tab1_resultgeral'])): ?>
                                                        <?= ($teste['tab1_resultgeral']) ?>
                                                    <?php else:
                                                        ?>0<?php endif; ?>
                                                </label></h4>
                                            <a id='btnsave' href="javascript:void(0);" onclick="validate()" class="btn green button-submit"style="float:right;">
                                                Salvar Teste <i class="m-icon-swapright m-icon-white"></i>
                                            </a>
                                            <a href="#tab4"  onclick="muda_botao('bntetp4')" data-toggle="tab" class="btn blue"style="float:left;">
                                                Voltar
                                            </a>

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/scripts/components-pickers.js') ?>"></script>


<script>

//                                                jQuery(document).ready(function () {
//                                                    ComponentsPickers.init();
//                                                    var date = new Date();
//                                                    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
//                                                    var end = new Date(date.getFullYear(), date.getMonth(), date.getDate());
//                                                    $('#evaldate').datepicker({
//                                                        format: "dd/mm/yyyy",
//                                                        todayHighlight: true,
//                                                        endDate: "today",
////                                                        startDate: today,
//                                                        autoclose: true,
//                                                    });
//                                                    $('#evaldatebtn').datepicker({
//                                                        format: "dd/mm/yyyy",
//                                                        todayHighlight: true,
//                                                        endDate: "today",
////                                                        startDate: today,
//                                                        autoclose: true,
//                                                    });
//                                                    $('#evaldate').datepicker('setDate', today);
//
//
//                                                });
                                                jQuery(document).ready(function () {
                                                    setTimeout(function () {
                                                        jQuery('#pro_med_dist').on('change', function () {
                                                            var total = Number(jQuery('#pro_med_dist').val()) + Number(jQuery('#pro_med_tresp').val());
                                                            var tresp = Number(jQuery('#pro_med_tresp').val()).toFixed(2);
                                                            var dist = Number(jQuery('#pro_med_dist').val()).toFixed(2);
                                                            total = total.toFixed(2);
                                                            jQuery('#pro_med_tresp').val(tresp);
                                                            jQuery('#pro_med_dist').val(dist);
                                                            jQuery('#pro_med_total').val(total);
                                                        });
                                                        jQuery('#pro_med_tresp').on('change', function () {
                                                            var total = Number(jQuery('#pro_med_dist').val()) + Number(jQuery('#pro_med_tresp').val());
                                                            var tresp = Number(jQuery('#pro_med_tresp').val()).toFixed(2);
                                                            var dist = Number(jQuery('#pro_med_dist').val()).toFixed(2);
                                                            total = total.toFixed(2);
                                                            jQuery('#pro_med_tresp').val(tresp);
                                                            jQuery('#pro_med_dist').val(dist);
                                                            jQuery('#pro_med_total').val(total);
                                                        });
                                                        jQuery('#dist_inter_incisivos').on('change', function () {
                                                            var total = Number(jQuery('#dist_inter_incisivos').val()) + Number(jQuery('#trespasse_vertical').val());
                                                            var dist_inter_incisivos = Number(jQuery('#dist_inter_incisivos').val()).toFixed(2);
                                                            var trespasse_vertical = Number(jQuery('#trespasse_vertical').val()).toFixed(2);
                                                            total = total.toFixed(2);
                                                            jQuery('#dist_inter_incisivos').val(dist_inter_incisivos);
                                                            jQuery('#trespasse_vertical').val(trespasse_vertical);
                                                            jQuery('#total_trespasse_incisivos').val(total);
                                                        });
                                                        jQuery('#trespasse_vertical').on('change', function () {
                                                            var total = Number(jQuery('#dist_inter_incisivos').val()) + Number(jQuery('#trespasse_vertical').val());
                                                            var dist_inter_incisivos = Number(jQuery('#dist_inter_incisivos').val()).toFixed(2);
                                                            var trespasse_vertical = Number(jQuery('#trespasse_vertical').val()).toFixed(2);
                                                            total = total.toFixed(2);
                                                            jQuery('#dist_inter_incisivos').val(dist_inter_incisivos);
                                                            jQuery('#trespasse_vertical').val(trespasse_vertical);
                                                            jQuery('#total_trespasse_incisivos').val(total);
                                                        });
                                                        jQuery('#linha_media_normal').on('ifChanged', function () {
                                                            if (jQuery('#linha_media_normal').is(':checked')) {
                                                                jQuery('#linha_media_direita').iCheck('uncheck');
                                                                jQuery('#linha_media_direita_mm').val('');
                                                                jQuery('#linha_media_esquerda').iCheck('uncheck');
                                                                jQuery('#linha_media_esquerda_mm').val('');
                                                                // inativa os outros
                                                                jQuery('#linha_media_direita').iCheck('disable');
                                                                jQuery('#linha_media_esquerda').iCheck('disable');
                                                            } else {
                                                                // ativa os outros
                                                                jQuery('#linha_media_direita').iCheck('enable');
                                                                jQuery('#linha_media_esquerda').iCheck('enable');

                                                            }
                                                        });
                                                        jQuery('#mov_fecham_normal').on('ifChanged', function () {
                                                            if (jQuery('#mov_fecham_normal').is(':checked')) {
//                                                                jQuery('#fech_dor_d').iCheck('uncheck');
//                                                                jQuery('#fech_dor_e').iCheck('uncheck');
//                                                                jQuery('[name=fech_desvio]').iCheck('uncheck');
                                                                jQuery("#fech_desvio").attr("disabled", "disabled");
//                                                                jQuery("#fech_dor").attr("disabled", "disabled");

                                                                // inativa os outros
                                                                //jQuery('#fech_dor_d').iCheck('disable');
                                                                //jQuery('#fech_dor_e').iCheck('disable');
//                                                                jQuery('[name=fech_desvio]').iCheck('disable');
                                                            } else {
                                                                // ativa os outros
//                                                                jQuery('#fech_dor_d').iCheck('enable');
//                                                                jQuery('#fech_dor_e').iCheck('enable');
//                                                                jQuery('[name=fech_desvio]').iCheck('enable');
                                                                jQuery("#fech_desvio").removeAttr("disabled");
                                                                jQuery("#fech_dor").removeAttr("disabled");

                                                            }
                                                        });
                                                        jQuery('#lat_medidas_dir').on('change', function () {
                                                            var lat_medidas_dir = Number(jQuery('#lat_medidas_dir').val()).toFixed(2);
                                                            jQuery('#lat_medidas_dir').val(lat_medidas_dir);
                                                        });
                                                        jQuery('#lat_medidas_esq').on('change', function () {
                                                            var lat_medidas_esq = Number(jQuery('#lat_medidas_esq').val()).toFixed(2);
                                                            jQuery('#lat_medidas_esq').val(lat_medidas_esq);
                                                        });
                                                        //abertura
                                                        jQuery('#mov_abert_normal').on('ifChanged', function () {
                                                            if (jQuery('#mov_abert_normal').is(':checked')) {
//                                                                jQuery("#aber_dor").attr("disabled", "disabled");
                                                                jQuery("#aber_desvio").attr("disabled", "disabled");
                                                                // inativa os outros
                                                                // jQuery('#aber_dor_d').iCheck('disable');
                                                                //jQuery('#aber_dor_e').iCheck('disable');
//                                                                jQuery('[name=aber_desvio]').iCheck('disable');
                                                            } else {
                                                                // ativa os outros
//                                                                jQuery('#aber_dor_d').iCheck('enable');
//                                                                jQuery('#aber_dor_e').iCheck('enable');
                                                                jQuery("#aber_desvio").removeAttr("disabled");
//                                                                jQuery("#aber_dor").removeAttr("disabled");
                                                            }
                                                        });
                                                        jQuery('[name=radioa4]').on('ifChanged', function () {
                                                            if (jQuery('[name=radioa4]:checked').val() == '3') {
                                                                jQuery('[name=radioa4_lado]').iCheck('uncheck');
                                                                // inativa os outros                   
                                                                jQuery('[name=radioa4_lado]').iCheck('disable');
                                                            } else {
                                                                // ativa os outros
                                                                jQuery('[name=radioa4_lado]').iCheck('enable');

                                                            }
                                                        });
                                                        jQuery('input').on('ifChanged', function (event) {

                                                            // calcula resultado da primeira aba
                                                            var tab1_result = 0;
                                                            if (Number(jQuery('[name=radioa1]:checked').val()) > 0) {
                                                                tab1_result += Number(jQuery('[name=radioa1]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radioa2]:checked').val()) > 0) {
                                                                tab1_result += Number(jQuery('[name=radioa2]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radioa3]:checked').val()) > 0) {
                                                                tab1_result += Number(jQuery('[name=radioa3]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radioa4]:checked').val()) > 0) {
                                                                tab1_result += Number(jQuery('[name=radioa4]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radioa5]:checked').val()) > 0) {
                                                                tab1_result += Number(jQuery('[name=radioa5]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radioa6]:checked').val()) > 0) {
                                                                tab1_result += Number(jQuery('[name=radioa6]:checked').val());
                                                            }
                                                            jQuery('#tab1_result').html(tab1_result);

                                                            // calcula resultado da segunda aba
                                                            var tab2_result = 0;
                                                            // primeiro teste
                                                            var tab2_1_result = 0;
                                                            if (Number(jQuery('[name=radioma1]:checked').val()) > 0) {
                                                                tab2_1_result += Number(jQuery('[name=radioma1]:checked').val());
                                                            }

                                                            if (Number(jQuery('[name=radioma2]:checked').val()) > 0) {
                                                                tab2_1_result += Number(jQuery('[name=radioma2]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radioma3]:checked').val()) > 0) {
                                                                tab2_1_result += Number(jQuery('[name=radioma3]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radioma4]:checked').val()) > 0) {
                                                                tab2_1_result += Number(jQuery('[name=radioma4]:checked').val());
                                                            }
                                                            jQuery('#tab2_1_result').html(tab2_1_result);
                                                            tab2_result += tab2_1_result;
                                                            //segundo teste
                                                            var tab2_2_result = 0;
                                                            if (Number(jQuery('[name=radiomb1]:checked').val()) > 0) {
                                                                tab2_2_result += Number(jQuery('[name=radiomb1]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomb2]:checked').val()) > 0) {
                                                                tab2_2_result += Number(jQuery('[name=radiomb2]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomb3]:checked').val()) > 0) {
                                                                tab2_2_result += Number(jQuery('[name=radiomb3]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomb4]:checked').val()) > 0) {
                                                                tab2_2_result += Number(jQuery('[name=radiomb4]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomb5]:checked').val()) > 0) {
                                                                tab2_2_result += Number(jQuery('[name=radiomb5]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomb6]:checked').val()) > 0) {
                                                                tab2_2_result += Number(jQuery('[name=radiomb6]:checked').val());
                                                            }
                                                            jQuery('#tab2_2_result').html(tab2_2_result);
                                                            tab2_result += tab2_2_result;
                                                            //terceiro teste
                                                            var tab2_3_result = 0;
                                                            if (Number(jQuery('[name=radiomc1]:checked').val()) > 0) {
                                                                tab2_3_result += Number(jQuery('[name=radiomc1]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomc2]:checked').val()) > 0) {
                                                                tab2_3_result += Number(jQuery('[name=radiomc2]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomc3]:checked').val()) > 0) {
                                                                tab2_3_result += Number(jQuery('[name=radiomc3]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomc4]:checked').val()) > 0) {
                                                                tab2_3_result += Number(jQuery('[name=radiomc4]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomc5]:checked').val()) > 0) {
                                                                tab2_3_result += Number(jQuery('[name=radiomc5]:checked').val());
                                                            }
                                                            jQuery('#tab2_3_result').html(tab2_3_result);
                                                            tab2_result += tab2_3_result;
                                                            //quarto teste
                                                            var tab2_4_result = 0;
                                                            if (Number(jQuery('[name=radiomd1]:checked').val()) > 0) {
                                                                tab2_4_result += Number(jQuery('[name=radiomd1]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomd2]:checked').val()) > 0) {
                                                                tab2_4_result += Number(jQuery('[name=radiomd2]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomd3]:checked').val()) > 0) {
                                                                tab2_4_result += Number(jQuery('[name=radiomd3]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiomd4]:checked').val()) > 0) {
                                                                tab2_4_result += Number(jQuery('[name=radiomd4]:checked').val());
                                                            }
                                                            jQuery('#tab2_4_result').html(tab2_4_result);
                                                            tab2_result += tab2_4_result;
                                                            // calcula total da segunda categoria
                                                            jQuery('#tab2_result').html(tab2_result);
                                                            // calcula resultado da terceira aba
                                                            var tab3_result = 0;
                                                            //respiracao
                                                            var tab3_1_result = 0;
                                                            if (Number(jQuery('[name=radiof1]:checked').val()) > 0) {
                                                                tab3_1_result += Number(jQuery('[name=radiof1]:checked').val());
                                                            }
                                                            jQuery('#tab3_1_result').html(tab3_1_result);
                                                            tab3_result += tab3_1_result;
                                                            //degluticao
                                                            var tab3_2_result = 0;
                                                            if (Number(jQuery('[name=radiof2]:checked').val()) > 0) {
                                                                tab3_2_result += Number(jQuery('[name=radiof2]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof3]:checked').val()) > 0) {
                                                                tab3_2_result += Number(jQuery('[name=radiof3]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof4]:checked').val()) > 0) {
                                                                tab3_2_result += Number(jQuery('[name=radiof4]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof5]:checked').val()) > 0) {
                                                                tab3_2_result += Number(jQuery('[name=radiof5]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof6]:checked').val()) > 0) {
                                                                tab3_2_result += Number(jQuery('[name=radiof6]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof7]:checked').val()) > 0) {
                                                                tab3_2_result += Number(jQuery('[name=radiof7]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof8]:checked').val()) > 0) {
                                                                tab3_2_result += Number(jQuery('[name=radiof8]:checked').val());
                                                            }
                                                            jQuery('#tab3_2_result').html(tab3_2_result);
                                                            tab3_result += tab3_2_result;
                                                            //mastigacao
                                                            var tab3_3_result = 0;
                                                            if (Number(jQuery('[name=radiof9]:checked').val()) > 0) {
                                                                tab3_3_result += Number(jQuery('[name=radiof9]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof10]:checked').val()) > 0) {
                                                                tab3_3_result += Number(jQuery('[name=radiof10]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof11]:checked').val()) > 0) {
                                                                tab3_3_result += Number(jQuery('[name=radiof11]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof12]:checked').val()) > 0) {
                                                                tab3_3_result += Number(jQuery('[name=radiof12]:checked').val());
                                                            }
                                                            if (Number(jQuery('[name=radiof13]:checked').val()) > 0) {
                                                                tab3_3_result += Number(jQuery('[name=radiof13]:checked').val());
                                                            }
                                                            jQuery('#tab3_3_result').html(tab3_3_result);
                                                            tab3_result += tab3_3_result;
                                                            jQuery('#tab3_result').html(tab3_result);
                                                            // linha media
                                                            if (jQuery('#linha_media_direita').is(':checked')) {
                                                                jQuery('.linha_media_direita_mm').show();
                                                                jQuery('#linha_media_direita_mm').on('change', function () {
                                                                    var linha_media_direita_mm = Number(jQuery('#linha_media_direita_mm').val()).toFixed(2);
                                                                    jQuery('#linha_media_direita_mm').val(linha_media_direita_mm);
                                                                });

                                                            } else {
                                                                jQuery('.linha_media_direita_mm').hide();
                                                                jQuery('#linha_media_direita_mm').val('');

                                                            }
                                                            if (jQuery('#linha_media_esquerda').is(':checked')) {
                                                                jQuery('.linha_media_esquerda_mm').show();
                                                                jQuery('#linha_media_esquerda_mm').on('change', function () {
                                                                    var linha_media_esquerda_mm = Number(jQuery('#linha_media_esquerda_mm').val()).toFixed(2);
                                                                    jQuery('#linha_media_esquerda_mm').val(linha_media_esquerda_mm);
                                                                });
                                                            } else {
                                                                jQuery('.linha_media_esquerda_mm').hide();
                                                                jQuery('#linha_media_esquerda_mm').val('');
                                                            }
                                                            jQuery('.tab1_resultgeral').html(tab1_result + tab2_result + tab3_result);
                                                            if ((jQuery('#radioa3_lv').is(':checked')) || jQuery('#radioa3_sv').is(':checked')) {
                                                                jQuery('#radioa3_volume_aumentado').show();
                                                            } else {
                                                                jQuery('[name=radioa3_volume_aumentado]:checked').val('');
                                                                jQuery('#radioa3_volume_aumentado').hide();
                                                            }
                                                            if ((jQuery('[name=radioa4]:checked').val() === '2') || (jQuery('[name=radioa4]:checked').val() === '1')) {
                                                                jQuery('#radioa4_lado_aumentado').show();
                                                            } else {
                                                                jQuery('[name=radioa4_radiovalue]:checked').val('');
                                                                jQuery('#radioa4_lado_aumentado').hide();
                                                            }
                                                            if ((jQuery('[name=radioa5]:checked').val() === '2') || (jQuery('[name=radioa5]:checked').val() === '1')) {
                                                                jQuery('#radioa5_local').show();
                                                            } else {
                                                                jQuery('[name=radioa5_local]:checked').val('');
                                                                jQuery('#radioa5_local').hide();
                                                            }

                                                            if ((jQuery('[name=radiof3]:checked').val() === '2') || (jQuery('[name=radiof3]:checked').val() === '1')) {
                                                                jQuery('#radiof3_local').show();
                                                            } else {
                                                                jQuery('[name=radiof3_local]:checked').val('');
                                                                jQuery('#radiof3_local').hide();
                                                            }


                                                            // movimentos de labiais - caso  falta de precisão/tremor

                                                            if ((jQuery('[name=radioma1]:checked').val() === '2') || (jQuery('[name=radioma2]:checked').val() === '2') || jQuery('[name=radioma3]:checked').val() === '2' || jQuery('[name=radioma4]:checked').val() === '2') {
                                                                jQuery('#especifique_radioma').show();
                                                                if (jQuery('[name=radioma1]:checked').val() === '2') {
                                                                    jQuery('#especifique_radioma1').show();
                                                                } else {
                                                                    jQuery('#especifique_radioma1').hide();
                                                                }
                                                                if (jQuery('[name=radioma2]:checked').val() === '2') {
                                                                    jQuery('#especifique_radioma2').show();
                                                                } else {
                                                                    jQuery('#especifique_radioma2').hide();
                                                                }
                                                                if (jQuery('[name=radioma3]:checked').val() === '2') {
                                                                    jQuery('#especifique_radioma3').show();
                                                                } else {
                                                                    jQuery('#especifique_radioma3').hide();
                                                                }
                                                                if (jQuery('[name=radioma4]:checked').val() === '2') {
                                                                    jQuery('#especifique_radioma4').show();
                                                                } else {
                                                                    jQuery('#especifique_radioma4').hide();
                                                                }

                                                            } else {
                                                                jQuery('#especifique_radioma').hide();
                                                                jQuery('#especifique_radioma1').hide();
                                                                jQuery('#especifique_radioma2').hide();
                                                                jQuery('#especifique_radioma3').hide();
                                                                jQuery('#especifique_radioma4').hide();

                                                            }


                                                            // movimentos de lingua - caso  falta de precisão/tremor

                                                            if ((jQuery('[name=radiomb1]:checked').val() === '2') || (jQuery('[name=radiomb2]:checked').val() === '2') || jQuery('[name=radiomb3]:checked').val() === '2' || jQuery('[name=radiomb4]:checked').val() === '2' || jQuery('[name=radiomb5]:checked').val() === '2' || jQuery('[name=radiomb6]:checked').val() === '2') {
                                                                jQuery('#especifique_radiomb').show();
                                                                if (jQuery('[name=radiomb1]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomb1').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomb1').hide();
                                                                }
                                                                if (jQuery('[name=radiomb2]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomb2').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomb2').hide();
                                                                }
                                                                if (jQuery('[name=radiomb3]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomb3').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomb3').hide();
                                                                }
                                                                if (jQuery('[name=radiomb4]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomb4').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomb4').hide();
                                                                }
                                                                if (jQuery('[name=radiomb5]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomb5').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomb5').hide();
                                                                }
                                                                if (jQuery('[name=radiomb6]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomb6').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomb6').hide();
                                                                }

                                                            } else {
                                                                jQuery('#especifique_radiomb').hide();
                                                                jQuery('#especifique_radiomb1').hide();
                                                                jQuery('#especifique_radiomb2').hide();
                                                                jQuery('#especifique_radiomb3').hide();
                                                                jQuery('#especifique_radiomb4').hide();
                                                                jQuery('#especifique_radiomb5').hide();
                                                                jQuery('#especifique_radiomb6').hide();

                                                            }


                                                            // movimentos de mandibula - caso  falta de precisão/desvio

                                                            if ((jQuery('[name=radiomc1]:checked').val() === '2') || (jQuery('[name=radiomc2]:checked').val() === '2') || jQuery('[name=radiomc3]:checked').val() === '2') {
                                                                jQuery('#desvio_radiomc').show();
                                                                if (jQuery('[name=radiomc1]:checked').val() === '2') {
                                                                    jQuery('#desvio_radiomc1').show();
                                                                } else {
                                                                    jQuery('#desvio_radiomc1').hide();
                                                                }
                                                                if (jQuery('[name=radiomc2]:checked').val() === '2') {
                                                                    jQuery('#desvio_radiomc2').show();
                                                                } else {
                                                                    jQuery('#desvio_radiomc2').hide();
                                                                }
                                                                if (jQuery('[name=radiomc3]:checked').val() === '2') {
                                                                    jQuery('#desvio_radiomc3').show();
                                                                } else {
                                                                    jQuery('#desvio_radiomc3').hide();
                                                                }
                                                            } else {
                                                                jQuery('#desvio_radiomc').hide();
                                                                jQuery('#desvio_radiomc1').hide();
                                                                jQuery('#desvio_radiomc2').hide();
                                                                jQuery('#desvio_radiomc3').hide();

                                                            }
                                                            // movimentos de bochecha - caso  falta de precisão/tremor

                                                            if ((jQuery('[name=radiomd1]:checked').val() === '2') || (jQuery('[name=radiomd2]:checked').val() === '2') || jQuery('[name=radiomd3]:checked').val() === '2' || jQuery('[name=radiomd4]:checked').val() === '2') {
                                                                jQuery('#especifique_radiomd').show();
                                                                if (jQuery('[name=radiomd1]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomd1').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomd1').hide();
                                                                }
                                                                if (jQuery('[name=radiomd2]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomd2').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomd2').hide();
                                                                }
                                                                if (jQuery('[name=radiomd3]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomd3').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomd3').hide();
                                                                }
                                                                if (jQuery('[name=radiomd4]:checked').val() === '2') {
                                                                    jQuery('#especifique_radiomd4').show();
                                                                } else {
                                                                    jQuery('#especifique_radiomd4').hide();
                                                                }

                                                            } else {
                                                                jQuery('#especifique_radiomd').hide();
                                                                jQuery('#especifique_radiomd1').hide();
                                                                jQuery('#especifique_radiomd2').hide();
                                                                jQuery('#especifique_radiomd3').hide();
                                                                jQuery('#especifique_radiomd4').hide();

                                                            }


                                                            if ((jQuery('#radiof10_pr').val() === '2' && jQuery('[name=radiof10]:checked').val() === '2') || jQuery('#radiof10_cr').is(':checked')) {
                                                                jQuery('#radiof10_lado').show();

                                                                jQuery('#radiof10_lado_percent').on('change', function () {
                                                                    var radiof10_lado_percent = Number(jQuery('#radiof10_lado_percent').val()).toFixed(2);
                                                                    jQuery('#radiof10_lado_percent').val(radiof10_lado_percent);
                                                                });

                                                            } else {
                                                                jQuery('#radiof10_lado').hide();
                                                            }
// funçao de desmarcar os radios
                                                            $("input[type=radio]").data('checkedStatus', true);
                                                            jQuery('input[type=radio]').on('ifClicked', function () {

                                                                if ($(this).data('checkedStatus') == true) {
                                                                    this.checked = false;
                                                                    $(this).removeAttr('checked').iCheck('update');
                                                                } else {
                                                                    $(this).attr('checked').iCheck('update');
                                                                    this.checked = true;
                                                                }

                                                            });

                                                        });
                                                    }, 1000);
                                                });

</script>
<script>


    function remove(array, element) {
        const index = array.indexOf(element);
        if (index !== -1) {
            array.splice(index, 1);
        }
    }


    function batata() {


//        var count = 0;
//        $("input[type=radio]").data('checkedStatus', true);
//        jQuery('input[type=radio]').each(function (i, radio) {
//            if ($(this).prop("checked")) {
//                $('.input-group').parent().removeClass('has-error').addClass('teste');
////                jQuery('.form-group').removeClass('has-error');
////                jQuery('.form-group').addClass('has-success');
//            } else {
//
//                $('.input-group').parent().removeClass('has-success').addClass('teste');
////                jQuery('.form-group').addClass('has-error');
////                jQuery('.form-group').removeClass('has-success');
//                count++;
//            }
//        });
//        console.log(count);


//        var acpp = jQuery('#acpp input').serializeArray();
//        var mob = jQuery('#mob input').serializeArray();
//        var func = jQuery('#funcoes input').serializeArray();
//        var analise = $("#analise").find("select,input").serializeArray();
//
//        $("#acpp").find("select, textarea, input").serializeArray();
//
//        $.each(acpp, function (i, acpp) {
//            if (!acpp.value)
//                alert(acpp.name + ' is required');
//        });
//
//
//
        $("input[type=radio]:checked").each(function () {
            if (this.value) {
//                jQuery('.form-group').removeClass('has-error');
//                jQuery('.form-group').addClass('has-success');
                $('.input-group').parent().removeClass('has-error').addClass('teste');

            } else {
//                jQuery('.form-group').addClass('has-error');
//                jQuery('.form-group').removeClass('has-success');
                $('.input-group').parent().removeClass('has-success').addClass('teste');

            }
        });

    }



    function validate() {

        if ((jQuery("#patient_id").val() === '')) {
            alertafalha('O campo com o nome do indivíduo avaliado não pode estar em branco!');
            return false;
        }
        var acpp = false;
        var mob = false;
        var func = false;
        var oclusao;
        var checked = $("#acpp :radio:checked");
        var groups = [];

        $("#acpp :radio").each(function () {
            if (groups.indexOf(this.name) < 0) {
                groups.push(this.name);
            }
        });

        if ((jQuery('#radioa3_lv').is(':checked')) || jQuery('#radioa3_sv').is(':checked')) {
        } else {
            remove(groups, 'radioa3_volume_aumentado');
        }

        if ((jQuery('[name=radioa4]:checked').val() === '2') || (jQuery('[name=radioa4]:checked').val() === '1')) {
        } else {
            remove(groups, 'radioa4_radiovalue');
        }

        if ((jQuery('[name=radioa5]:checked').val() === '2') || (jQuery('[name=radioa5]:checked').val() === '1')) {
        } else {
            remove(groups, 'radioa5_local');
        }
        if (groups.length == checked.length) {
            acpp = true;
        } else {
            var total = groups.length - checked.length;
            var a = total > 1 ? ' perguntas ' : ' pergunta ';
            acpp = 'Aparência e condição postural: ' + total + a + 'não preenchida(s)';
        }
        //mobilidade
        var checked = $("#mob :radio:checked");
        var groups = [];

        $("#mob :radio").each(function () {
            if (groups.indexOf(this.name) < 0) {
                groups.push(this.name);
            }
        });
        if (groups.length == checked.length) {
            mob = true;
        } else {
            var total = groups.length - checked.length;
            var a = total > 1 ? ' perguntas ' : ' pergunta ';
            mob = 'Mobilidade: ' + total + a + 'não preenchida(s)';
        }

        //funções
        var checked = $("#funcoes :radio:checked");
        var groups = [];

        $("#funcoes :radio").each(function () {
            if (groups.indexOf(this.name) < 0) {
                groups.push(this.name);
            }
        });



        if ((jQuery('[name=radiof3]:checked').val() === '2') || (jQuery('[name=radiof3]:checked').val() === '1')) {
        } else {
            remove(groups, 'radiof3_local');
        }
        if ((jQuery('[name=radiof10]:checked').val() === '2') || (jQuery('[name=radiof10]:checked').val() === '1')) {
        } else {
            remove(groups, 'radiof10_lado');
        }

        if (groups.length == checked.length) {
            func = true;
        } else {
            var total = groups.length - checked.length;
            var a = total > 1 ? ' perguntas ' : ' pergunta ';
            func = 'Funções: ' + total + a + 'não preenchida(s) ';
        }

        if (acpp === true && mob === true && func === true) {
            save_test();
        } else {
            if (acpp === true) {
                acpp = '';
            }
            if (mob === true) {
                mob = '';
            }
            if (func === true) {
                func = '';
            }

            swal({
                title: "<h3>Tem certeza que deseja prosseguir?</h3>",
                html: "<h4><strong>Há itens que não foram preenchidos!</strong></h4><br><pre>" + acpp + "<br>" + mob + "<br>" + func + "</pre>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#00ce0d',
                cancelButtonColor: '#d33',
                confirmButtonText: "Sim, continuar e salvar!",
                cancelButtonText: "Não, cancelar e voltar!",
                customClass: 'alert_swal'
//                timer: 10000,
            }).then(function (isConfirm) {
                if (isConfirm.value == true) {
                    swal({
                        title: 'Avaliação salva!',
                        text: 'Avaliação inserida com sucesso!'
                    }).then(function () {
                        save_test(); // <--- submit form programmatically
                    });
                } else {
                    swal("Cancelado", "Retornando a avaliação...)", "error");
                }
            });
        }

    }


    function save_test() {
        jQuery('#btnsave').attr('disable', 'disable');
        var radioa1_radiovalue = 0;
        if (jQuery('#radioa1_aa:checked').val() === '2') {
            radioa1_radiovalue = 'atividade_aumentada';
        } else if (jQuery('#radioa1_dl:checked').val() === '2') {
            radioa1_radiovalue = 'disfuncao_leve';
        }
        var radioa2_radiovalue = 0;
        if (jQuery('#radioa2_se:checked').val() === '2') {
            radioa2_radiovalue = 'sem_espaco';
        } else if (jQuery('#radioa2_dl:checked').val() === '2') {
            radioa2_radiovalue = 'disfuncao_leve';
        }
        var radioa3_radiovalue = 0;
        var radioa3_volume_aumentado = 0;
        if (jQuery('#radioa3_lv:checked').val() === '2') {
            radioa3_radiovalue = 'leve_volume';
            radioa3_volume_aumentado = (jQuery('[name=radioa3_volume_aumentado]:checked').val());
        } else if (jQuery('#radioa3_sv:checked').val() === '1') {
            radioa3_radiovalue = 'severa_volume';
            radioa3_volume_aumentado = (jQuery('[name=radioa3_volume_aumentado]:checked').val());
        } else if (jQuery('#radioa3_lf:checked').val() === '2') {
            radioa3_radiovalue = 'leve_flacida';
        } else if (jQuery('#radioa3_sf:checked').val() === '1') {
            radioa3_radiovalue = 'severa_flacida';
        }
        var radioa4_radiovalue = 0;
        if (jQuery('[name=radioa4]:checked').val() === '1' || jQuery('[name=radioa4]:checked').val() === '2') {
            radioa4_radiovalue = (jQuery('[name=radioa4_radiovalue]:checked').val());
        }
        var radiof10_radiovalue = 0;
        var radiof10_lado = 0;
        if (jQuery('#radiof10_nt:checked').val() === '1') {
            radiof10_radiovalue = 'nao_tritura';
        } else if (jQuery('#radiof10_af:checked').val() === '1') {
            radiof10_radiovalue = 'anterior_frontal';
        } else if (jQuery('#radiof10_cr:checked').val() === '1') {
            radiof10_radiovalue = 'cronica';
            radiof10_lado = jQuery('[name=radiof10_lado]:checked').val();
        } else if (jQuery('#radiof10_pr:checked').val() === '2') {
            radiof10_radiovalue = 'preferencial';
            radiof10_lado = jQuery('[name=radiof10_lado]:checked').val();
        }

        var radiofoutros = 0;
        radiofoutros += Number(jQuery('[name=radiof4]:checked').val());
        radiofoutros += Number(jQuery('[name=radiof5]:checked').val());
        radiofoutros += Number(jQuery('[name=radiof6]:checked').val());

        var radiofeficiencia = 0;
        radiofeficiencia += Number(jQuery('[name=radiof7]:checked').val());
        radiofeficiencia += Number(jQuery('[name=radiof8]:checked').val());

        var radiofoutros2 = 0;
        radiofoutros2 += Number(jQuery('[name=radiof11]:checked').val());
        radiofoutros2 += Number(jQuery('[name=radiof12]:checked').val());
        radiofoutros2 += Number(jQuery('[name=radiof13]:checked').val());
// value movimentos labiais - caso falta precisão/tremor
        var especifique_radioma1_val = 0;
        var especifique_radioma2_val = 0;
        var especifique_radioma3_val = 0;
        var especifique_radioma4_val = 0;

        if (jQuery('[name=radioma1]:checked').val() === '2') {
            especifique_radioma1_val = jQuery("#especifique_radioma1_val").val();
        }
        if (jQuery('[name=radioma2]:checked').val() === '2') {
            especifique_radioma2_val = jQuery("#especifique_radioma2_val").val();
        }
        if (jQuery('[name=radioma3]:checked').val() === '2') {
            especifique_radioma3_val = jQuery("#especifique_radioma3_val").val();
        }
        if (jQuery('[name=radioma4]:checked').val() === '2') {
            especifique_radioma4_val = jQuery("#especifique_radioma4_val").val();
        }
        // value movimentos da lingua - caso falta precisão/tremor
        var especifique_radiomb1_val = 0;
        var especifique_radiomb2_val = 0;
        var especifique_radiomb3_val = 0;
        var especifique_radiomb4_val = 0;
        var especifique_radiomb5_val = 0;
        var especifique_radiomb6_val = 0;

        if (jQuery('[name=radiomb1]:checked').val() === '2') {
            especifique_radiomb1_val = jQuery("#especifique_radiomb1_val").val();
        }
        if (jQuery('[name=radiomb2]:checked').val() === '2') {
            especifique_radiomb2_val = jQuery("#especifique_radiomb2_val").val();
        }
        if (jQuery('[name=radiomb3]:checked').val() === '2') {
            especifique_radiomb3_val = jQuery("#especifique_radiomb3_val").val();
        }
        if (jQuery('[name=radiomb4]:checked').val() === '2') {
            especifique_radiomb4_val = jQuery("#especifique_radiomb4_val").val();
        }
        if (jQuery('[name=radiomb5]:checked').val() === '2') {
            especifique_radiomb5_val = jQuery("#especifique_radiomb5_val").val();
        }
        if (jQuery('[name=radiomb6]:checked').val() === '2') {
            especifique_radiomb6_val = jQuery("#especifique_radiomb6_val").val();
        }
// value movimentos da mandiula - caso falta precisão/desvio

        var desvio_radiomc1_val = 0;
        var desvio_radiomc2_val = 0;
        var desvio_radiomc3_val = 0;
        if (jQuery('[name=radiomc1]:checked').val() === '2') {
            desvio_radiomc1_val = jQuery("#desvio_radiomc1_val").val();
        }
        if (jQuery('[name=radiomc2]:checked').val() === '2') {
            desvio_radiomc2_val = jQuery("#desvio_radiomc2_val").val();
        }
        if (jQuery('[name=radiomc3]:checked').val() === '2') {
            desvio_radiomc3_val = jQuery("#desvio_radiomc3_val").val();
        }
// value movimentos da bocheca - caso falta precisão/tremor

        var especifique_radiomd1_val = 0;
        var especifique_radiomd2_val = 0;
        var especifique_radiomd3_val = 0;
        var especifique_radiomd4_val = 0;

        if (jQuery('[name=radiomd1]:checked').val() === '2') {
            especifique_radiomd1_val = jQuery("#especifique_radiomd1_val").val();
        }
        if (jQuery('[name=radiomd2]:checked').val() === '2') {
            especifique_radiomd2_val = jQuery("#especifique_radiomd2_val").val();
        }
        if (jQuery('[name=radiomd3]:checked').val() === '2') {
            especifique_radiomd3_val = jQuery("#especifique_radiomd3_val").val();
        }
        if (jQuery('[name=radiomd4]:checked').val() === '2') {
            especifique_radiomd4_val = jQuery("#especifique_radiomd4_val").val();
        }

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "home/save_test",
            type: "post",
            dataType: 'json',
            data: {
                patient_id: jQuery("#patient_id").val(),
                evaldate: jQuery("#evaldate").val(),
                startproblem: jQuery("[name=startproblem]").val(),
                queixa: jQuery("[name=queixa]").val(),
                observacoes_acpp: jQuery("[name=observacoes_acpp]").val(),
                observacoes_mobilidade: jQuery("[name=observacoes_mobilidade]").val(),
                observacoes_funcoes: jQuery("[name=observacoes_funcoes]").val(),
                observacoes_analise: jQuery("[name=observacoes_analise]").val(),
                radioa1: jQuery("[name=radioa1]:checked").val(),
                radioa1_radiovalue: radioa1_radiovalue,
                radioa2: jQuery("[name=radioa2]:checked").val(),
                radioa2_radiovalue: radioa2_radiovalue,
                radioa3: jQuery("[name=radioa3]:checked").val(),
                radioa3_radiovalue: radioa3_radiovalue,
                radioa3_volume_aumentado: radioa3_volume_aumentado,
                radioa4: jQuery("[name=radioa4]:checked").val(),
                radioa4_radiovalue: radioa4_radiovalue,
                radioa5: jQuery("[name=radioa5]:checked").val(),
                radioa5_local: jQuery("[name=radioa5_local]:checked").val(),
                radioa6: jQuery("[name=radioa6]:checked").val(),
                tab1_result: jQuery("#tab1_result").html(),
                radioma1: jQuery("[name=radioma1]:checked").val(),
                radioma2: jQuery("[name=radioma2]:checked").val(),
                radioma3: jQuery("[name=radioma3]:checked").val(),
                radioma4: jQuery("[name=radioma4]:checked").val(),
                especifique_radioma1_val: especifique_radioma1_val,
                especifique_radioma2_val: especifique_radioma2_val,
                especifique_radioma3_val: especifique_radioma3_val,
                especifique_radioma4_val: especifique_radioma4_val,
                tab2_1_result: jQuery("#tab2_1_result").html(),
                radiomb1: jQuery("[name=radiomb1]:checked").val(),
                radiomb2: jQuery("[name=radiomb2]:checked").val(),
                radiomb3: jQuery("[name=radiomb3]:checked").val(),
                radiomb4: jQuery("[name=radiomb4]:checked").val(),
                radiomb5: jQuery("[name=radiomb5]:checked").val(),
                radiomb6: jQuery("[name=radiob6]:checked").val(),
                especifique_radiomb1_val: especifique_radiomb1_val,
                especifique_radiomb2_val: especifique_radiomb2_val,
                especifique_radiomb3_val: especifique_radiomb3_val,
                especifique_radiomb4_val: especifique_radiomb4_val,
                especifique_radiomb5_val: especifique_radiomb5_val,
                especifique_radiomb6_val: especifique_radiomb6_val,
                tab2_2_result: jQuery("#tab2_2_result").html(),
                radiomc1: jQuery("[name=radiomc1]:checked").val(),
                desvio_radiomc1_val: desvio_radiomc1_val,
                desvio_radiomc2_val: desvio_radiomc2_val,
                desvio_radiomc3_val: desvio_radiomc3_val,
                radiomc2: jQuery("[name=radiomc2]:checked").val(),
                radiomc3: jQuery("[name=radiomc3]:checked").val(),
                radiomc4: jQuery("[name=radiomc4]:checked").val(),
                radiomc5: jQuery("[name=radiomc5]:checked").val(),
                tab2_3_result: jQuery("#tab2_3_result").html(),
                radiomd1: jQuery("[name=radiomd1]:checked").val(),
                radiomd2: jQuery("[name=radiomd2]:checked").val(),
                radiomd3: jQuery("[name=radiomd3]:checked").val(),
                radiomd4: jQuery("[name=radiomd4]:checked").val(),
                especifique_radiomd1_val: especifique_radiomd1_val,
                especifique_radiomd2_val: especifique_radiomd2_val,
                especifique_radiomd3_val: especifique_radiomd3_val,
                especifique_radiomd4_val: especifique_radiomd4_val,
                tab2_4_result: jQuery("#tab2_4_result").html(),
                tab2_result: jQuery("#tab2_result").html(),
                radiof1: jQuery("[name=radiof1]:checked").val(),
                tab3_1_result: jQuery("#tab3_1_result").html(),
                radiof2: jQuery("[name=radiof2]:checked").val(),
                radiof3: jQuery("[name=radiof3]:checked").val(),
                radiof4: jQuery("[name=radiof4]:checked").val(),
                radiof5: jQuery("[name=radiof5]:checked").val(),
                radiof6: jQuery("[name=radiof6]:checked").val(),
                radiof7: jQuery("[name=radiof7]:checked").val(),
                radiof8: jQuery("[name=radiof8]:checked").val(),
                radiofoutros: radiofoutros,
                radiofeficiencia: radiofeficiencia,
                tab3_2_result: jQuery("#tab3_2_result").html(),
                radiof9: jQuery("[name=radiof9]:checked").val(),
                radiof10: jQuery("[name=radiof10]:checked").val(),
                radiof10_radiovalue: radiof10_radiovalue,
                radiof10_lado: radiof10_lado,
                radiof10_lado_percent: jQuery("#radiof10_lado_percent").val(),
                radiof11: jQuery("[name=radiof11]:checked").val(),
                radiof12: jQuery("[name=radiof12]:checked").val(),
                radiof13: jQuery("[name=radiof13]:checked").val(),
                food: jQuery("#food").val(),
                spent_time_food: jQuery("#spent_time_food").val(),
                radiofoutros2: radiofoutros2,
                tab3_3_result: jQuery("#tab3_3_result").html(),
                tab3_result: jQuery("#tab3_result").html(),
                radioaod: jQuery("[name=radioaod]:checked").val(),
                radioaoe: jQuery("[name=radioaoe]:checked").val(),
                linha_media_normal: jQuery("#linha_media_normal").is(':checked'),
                linha_media_direita_mm: jQuery("#linha_media_direita_mm").val(),
                linha_media_esquerda_mm: jQuery("#linha_media_esquerda_mm").val(),
                mov_abert_normal: jQuery("#mov_abert_normal").is(':checked'),
                mov_fecham_normal: jQuery("#mov_fecham_normal").is(':checked'),
                aber_dor_d: jQuery("#aber_dor_d").is(':checked'),
                aber_dor_e: jQuery("#aber_dor_e").is(':checked'),
                fech_dor_d: jQuery("#fech_dor_d").is(':checked'),
                fech_dor_e: jQuery("#fech_dor_e").is(':checked'),
                fech_dor: jQuery("#fech_dor").val(),
                aber_dor: jQuery("#aber_dor").val(),
                aber_desvio: jQuery("#aber_desvio").val(),
                fech_desvio: jQuery("#fech_desvio").val(),
                trespasse_vertical: jQuery("#trespasse_vertical").val(),
                dist_inter_incisivos: jQuery("#dist_inter_incisivos").val(),
                total_trespasse_incisivos: jQuery("#total_trespasse_incisivos").val(),
                lat_dir_dor_d: jQuery("#lat_dir_dor_d").is(':checked'),
                lat_dir_dor_e: jQuery("#lat_dir_dor_e").is(':checked'),
                lat_dir_dor: jQuery("#lat_dir_dor").val(),
                lat_esq_dor_d: jQuery("#lat_esq_dor_d").is(':checked'),
                lat_esq_dor_e: jQuery("#lat_esq_dor_e").is(':checked'),
                lat_esq_dor: jQuery("#lat_esq_dor").val(),
                lat_guia_dir: jQuery("#lat_guia_dir").val(),
                lat_guia_esq: jQuery("#lat_guia_esq").val(),
                lat_int_trab_dir: jQuery("#lat_int_trab_dir").val(),
                lat_int_trab_esq: jQuery("#lat_int_trab_esq").val(),
                lat_int_bal_dir: jQuery("#lat_int_bal_dir").val(),
                lat_int_bal_esq: jQuery("#lat_int_bal_esq").val(),
                lat_medidas_dir: jQuery("#lat_medidas_dir").val(),
                lat_medidas_esq: jQuery("#lat_medidas_esq").val(),
                pro_mov_dor_d: jQuery("#pro_mov_dor_d").is(':checked'),
                pro_mov_dor_e: jQuery("#pro_mov_dor_e").is(':checked'),
                pro_mov_dor: jQuery("#pro_mov_dor").val(),
                pro_mov_desv: jQuery("#pro_mov_desv").val(),
                pro_interf_d: jQuery("#pro_interf_d").is(':checked'),
                pro_interf_e: jQuery("#pro_interf_e").is(':checked'),
                pro_interf: jQuery("#pro_interf").val(),
                pro_med_tresp: jQuery("#pro_med_tresp").val(),
                pro_med_dist: jQuery("#pro_med_dist").val(),
                pro_med_total: jQuery("#pro_med_total").val(),
                atm_abert_dir: jQuery("#atm_abert_dir").val(),
                atm_abert_esq: jQuery("#atm_abert_esq").val(),
                atm_fech_dir: jQuery("#atm_fech_dir").val(),
                atm_fech_esq: jQuery("#atm_fech_esq").val(),
                atm_prot_dir: jQuery("#atm_prot_dir").val(),
                atm_prot_esq: jQuery("#atm_prot_esq").val(),
                atm_lat_dir_dir: jQuery("#atm_lat_dir_dir").val(),
                atm_lat_dir_esq: jQuery("#atm_lat_dir_esq").val(),
                atm_lat_esq_dir: jQuery("#atm_lat_esq_dir").val(),
                atm_lat_esq_esq: jQuery("#atm_lat_esq_esq").val(),
                tab1_resultgeral: jQuery("#result_geral").text()
            },
            success: function (response) {
                if (response.status == "OK") {
                    setTimeout(alertasucesso(), 2000);
                    window.location = jQuery("body").data('baseurl') + 'home/list_tests';
                }
            }
        });

    }
</script>


<script>
    function muda_botao(id) {
        jQuery('#' + id).click();
        $(window).scrollTop(0);
    }

</script>
<style>
    .alert_swal {
        background-color: #ffffff;
        width: 750px;
        padding: 17px;
        border-radius: 5px;
        text-align: center;
        position: fixed;
        margin-left: -256px;
        margin-top: -200px;
        overflow: hidden;
        display: none;
        z-index: 2000;
    }

    .swal2-modal pre {
        background-color: #04008c;
        color: #f7f7f7;
        padding: 10px;
        font-size: 14px;
        font: 14px arial, verdana, helvetica, sans-serif; 
        font-weight: bold;
    }
    .teste {
        font-weight: bold;
        color: #333;
        background-color: #ffc;
    }

    .red-class {
        background-color: red;
    }

    textarea {
        resize: none;
    }

    .imgefeito {
        margin: 0;
        overflow: hidden;
        float: left;
        position: relative;
    }
    .imgefeito a {
        text-decoration: none;
        float: left;
    }
    .imgefeito a:hover {
        cursor: pointer;
        .imgefeito a:hover .desc{
            display: block;
            font-size: 1.2em;
            padding: 10px 0;
            background: #111;
            filter:alpha(opacity=75);
            opacity:.75;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=75)"; /*--IE 8 Transparency--*/
            color: #fff;
            position: absolute;
            bottom: 11px;
            left: 11px;
            padding: 10px;
            margin: 0;
            width: 615px;
            border-top: 1px solid #999;
        }
        .imgefeito a:hover .desc strong {
            display: block;
            margin-bottom: 5px;
            font-size:1.5em;
        }
    } 
</style>

<style type="text/css">
    .formata { /* esta classe é somente 
              para formatar a fonte */
        font: 12px arial, verdana, helvetica, sans-serif; 
        font-weight: bold;
    }

    a.dcontexto{
        position:relative; 
        padding:0;
        color:#FFFFFF;
        text-decoration:none;
        cursor:help; 
        z-index:20;
    }
    a.dcontexto:hover{
        background:transparent;
        z-index:21; 
    }
    a.dcontexto span{display: none}
    a.dcontexto:hover span{ 
        display:block;
        position:absolute;
        width:260px; 
        top:2em;
        right-align:justify;
        left:0;
        font: 12px arial, verdana, helvetica, sans-serif; 
        padding:5px 10px;
        border:1px solid #999;
        background:#3598dc; 
        color:#FFFFFF;
        text-decoration:none;
    }
</style>

<script src="<?= $this->config->base_url(JSPATH . 'bootstrapAlert.js') ?>" type="text/javascript"></script>
<script src="<?= $this->config->base_url(JSPATH . 'bootstrapAlert.min.js') ?>" type="text/javascript"></script>
<script>


    function alertasucesso() {
        BootstrapAlert.success({
            title: "Avaliação inserida com sucesso!",
            message: "OK"
        });
    }
    function alertafalha(msg) {
        BootstrapAlert.alert({
            title: "Atenção!",
            message: msg
        });
    }



    //        if (jQuery('#acpp input').serializeArray().length != 6) {// 6;
//
//            alertafalha('Por favor, preencha todos os campos na etapa 1!');
//
//            return false;
//        }
//        if (jQuery('#mob input').serializeArray().length != 19) {//19
//            alertafalha('Por favor, preencha todos os campos na etapa 2!');
//
//            return false;
//
//        }
//        if (jQuery('#funcoes input').serializeArray().length != 13) {
//            alertafalha('Por favor, preencha todos os campos na etapa 3!');
//
//            return false;
//
//        }
////        // RADIOBOX
//        if ((jQuery('#analise input').serializeArray().length < 3)) {
//            alertafalha('Por favor, preencha todos os campoas na etapa 4!');
//            return false;
//        }
//        // LINHA MEDIA
//        if (!(jQuery('#linha_media_normal').is(':checked'))
//                && !(jQuery('#linha_media_direita').is(':checked'))
//                && !(jQuery('#linha_media_esquerda').is(':checked'))) {
//            alertafalha('Por favor, preencha todos os campoas na etapa 4!');
//            return false;
//        }
////        // trepasse
//        if (jQuery('#trespasse_vertical').val() == '' || jQuery('#dist_inter_incisivos').val() == '') {
//            alertafalha('Por favor, preencha todos os campoas na etapa 4!');
//            return false;
//        }
//        // medidas
//        if ((jQuery('#lat_medidas_dir').val() == "") && (jQuery('#lat_medidas_esq').val() == "") && (jQuery('#pro_med_tresp') == "").val() && (jQuery('#pro_med_dist').val() == "")) {
//            alertafalha('Por favor, preencha todos os campoas na etapa 4!');
//            return false;
//        }

</script>