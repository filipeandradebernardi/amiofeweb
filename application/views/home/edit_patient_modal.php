<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Editar usuário</h4>
</div>
<div class="modal-body">
    <form role="form">
        <div class="form-body">
            <div class="row">
                <div class="form-group col-md-6 name">
                    <label>Nome do paciente*</label>
                    <div class="input-icon input-icon-lg">
                        <i class="fa fa-user"></i>
                        <input type="text" id="name" class="form-control input-lg" placeholder="Nome do paciente" value="<?= $patient['name'] ?>">
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Nome da mãe</label>
                    <div class="input-icon input-icon-lg">
                        <i class="fa fa-user"></i>
                        <input type="text" id="mother_name" class="form-control input-lg" placeholder="Nome da mãe" value="<?= $patient['mother_name'] ?>">
                    </div>                       
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6 telephone">
                    <label>Telefone*</label>
                    <div class="input-icon input-icon-lg">
                        <i class="fa fa-phone"></i>
                        <input type="text" id="telephone" class="form-control input-lg" placeholder="Telefone" value="<?= $patient['telephone'] ?>">
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Celular</label>
                    <div class="input-icon input-icon-lg">
                        <i class="fa fa-phone"></i>
                        <input type="text" id="cellphone" class="form-control input-lg" placeholder="Celular" value="<?= $patient['cellphone'] ?>">
                    </div>                       
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6 city">
                    <label>Cidade*</label>
                    <div class="input-icon input-icon-lg">
                        <i class="fa fa-phone"></i>
                        <input type="text" id="city" class="form-control input-lg" placeholder="Cidade" value="<?= $patient['city'] ?>">
                    </div>
                </div>

                <div class="form-group col-md-6 street">
                    <label>Rua*</label>
                    <div class="input-icon input-icon-lg">
                        <i class="fa fa-phone"></i>
                        <input type="text" id="street" class="form-control input-lg" placeholder="Rua" value="<?= $patient['street'] ?>">
                    </div>                       
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6 number">
                    <label>Número*</label>
                    <div class="input-icon input-icon-lg">
                        <i class="fa fa-phone"></i>
                        <input type="number" id="number" class="form-control input-lg" placeholder="Número" value="<?= $patient['number'] ?>">
                    </div>
                </div>

                <div class="form-group col-md-6">
                    <label>Complemento</label>
                    <div class="input-icon input-icon-lg">
                        <i class="fa fa-phone"></i>
                        <input type="text" id="complement" class="form-control input-lg" placeholder="Complemento" value="<?= $patient['complement'] ?>">
                    </div>                       
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-6">
                    <label>Genero</label>
                    <select class="form-control input-lg" id="gender">
                        <option value="male" <?php
                        if ($patient['gender'] == 'male'): echo 'selected';
                        endif;
                        ?>>Masculino</option>
                        <option value="female" <?php
                                if ($patient['gender'] == 'female'): echo 'selected';
                                endif;
                                ?>>Feminino</option>
                    </select>
                </div>

                <div class="form-group col-md-6 birthdate">
                    <label>Data de nascimento*</label>
                    <div class="input-group date date-picker " data-date-format="yyyy-mm-dd">
                        <input type="text" class="form-control input-lg" readonly id="birthdate" value="<?= $patient['birthdate'] ?>">
                        <span class="input-group-btn">
                            <button class="btn default input-lg" type="button"><i class="fa fa-calendar"></i></button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Fechar</button>
    <a href="javascript:void(0)" onclick="save_edited_patient(<?= $patient['id'] ?>)" class="btn blue">Salvar</a>
</div>