<!DOCTYPE html>
<html lang="en">
    <head>
        <title>AMIOFEWEB - Entrar</title>
        <meta charset="UTF-8">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->	
        <link href="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/images/icons/favicon.ico') ?>" rel="stylesheet" type="text/css"/>
        <!--===============================================================================================-->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>

        <!--===============================================================================================-->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/fonts/font-awesome-4.7.0/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>

        <!--===============================================================================================-->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/vendor/animate/animate.css') ?>" rel="stylesheet" type="text/css"/>

        <!--===============================================================================================-->	
        <link href="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/vendor/css-hamburgers/hamburgers.min.css') ?>" rel="stylesheet" type="text/css"/>

        <!--===============================================================================================-->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/vendor/select2/select2.min.css') ?>" rel="stylesheet" type="text/css"/>

        <!--===============================================================================================-->
        <link href="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/css/util.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/css/main.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="https://fonts.googleapis.com/css?family=Montserrat:100" rel="stylesheet">
        <link rel="stylesheet" href="<?= $this->config->base_url(ASSETSPATH . 'pnotify/pnotify.custom.css') ?> " />

        <!--===============================================================================================-->
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login" data-baseurl="<?= $this->config->base_url(); ?>">

        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div class="text-center js-tilt" style="padding-bottom: 45px;">
                        <h2 class="fadeInUp animation-delay4" style="font-weight:bold">
                            <span class="text-success" style="font-size: 54px;font-family:Asap"><i>AMIOFEWEB</i></span></br>
                            <span style="font-size: 14px;color:#808080; text-shadow:0 1px #fff">
                                Protocolo de Avaliação Miofuncional Orofacial com Escores Informatizado: Versão WEB</span>
                        </h2>
                    </div>                
                    <form class="login100-form validate-form">
<!--                        <span class="login100-form-title">
                            Acesse sua conta
                        </span>-->

                        <div class="wrap-input100 validate-input" data-validate = "É necessário um email válido ex@abc.xyz">
                            <input class="input100" onkeyup="login_enter(event);" type="text"id="username" name="email" placeholder="Email">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate = "É necessário uma senha">
                            <input class="input100" onkeyup="login_enter(event);" type="password" id="password" name="pass" placeholder="Senha">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div>

                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn" onclick="login()">
                                ENTRAR
                            </button>
                        </div>
                        <div class="container-login100-form-btn">
                            <span class="txt2">
                                Mantenha-me contectado                
                                <input type="checkbox"id="RememberMe">

                            </span>
                        </div>
                        <div class="text-center p-t-12">
                            <span class="txt1">
                                Esqueci meu
                            </span>
                            <a class="txt2" href="#modalForgotPassword">
                                Usuário / Senha?
                            </a>
                        </div>

                        <div class="text-center p-t-12">
                            <label>Não tem conta?</label>

                            <a class="txt2" href="<?= $this->config->base_url('login/register') ?>">
                                CADASTRE-SE
                                <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--===============================================================================================-->	
        <script src="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/vendor/jquery/jquery-3.2.1.min.js') ?>"></script>

        <!--===============================================================================================-->

        <script src="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/vendor/bootstrap/js/popper.js') ?>"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>

        <!--===============================================================================================-->
        <script src="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/vendor/select2/select2.min.js') ?>"></script>

        <!--===============================================================================================-->
        <script src="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/vendor/tilt/tilt.jquery.min.js') ?>"></script>

        <script >
                                $('.js-tilt').tilt({
                                    scale: 1.1
                                })
        </script>
        <!--===============================================================================================-->
        <script src="<?= $this->config->base_url(ASSETSPATH . 'Login_v1/js/main.js') ?>"></script>

        <script src="<?= $this->config->base_url(JSPATH . 'login.js') ?>"></script>

        <script src="<?= $this->config->base_url(ASSETSPATH . 'pnotify/pnotify.custom.js') ?>"></script>   



    </body>

    <!-- END BODY -->
</html>