<h3 class="page-title"><?= $title ?> </h3>

<section class="panel">
    <div class="panel-body">
        <a href="<?= $this->config->base_url('home/new_patient') ?>" class="btn btn-success pull-left"><i class="fa fa-plus-circle"></i> Cadastrar Indivíduo</a>
        <table class="table table-bordered table-striped mb-none" id="table_patient">
            <thead>
                <tr>
                    <th>Indivíduo</th>
                    <th>Idade</th>
                    <th>Endereço</th>
                    <th>Contato</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php if (isset($patients)): ?>
                    <?php foreach ($patients as $p): ?>
                        <tr class="content_row">
                            <td><?= ($p['name']) ?></td>
                            <td><?= $p['age'] ?></td>
                            <td><?= ($p['street']) ?>, <?= ($p['number']) ?> - <?= ($p['city']) ?></td>
                            <td><?= ($p['telephone']) ?> - <?= ($p['cellphone']) ?></td>
                            <td>
                                <a href="javascript:void(0)" title="Nova avaliação do indivíduo" onclick="<?= $this->config->base_url('home/new_test') ?>"><i class="fa fa-plus-circle"></i></a>
                                <a class="btn "title="Editar dados do indivíduo" href="<?= $this->config->base_url('home/edit_patient') . '/' . $p['id'] ?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-pencil-square"></i></a>
                                <a href="javascript:void(0)" onclick="return confirm('Certeza que deseja excluir este indivíduo?') ? delete_patient(<?= $p['id'] ?>) : '';" title="Deletar indivíduo"><i class="fa fa-trash-o"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</section>


<script>
    $(document).ready(function () {
        $('#table_patient').DataTable({
//            "scrollY": 400,
            "scrollX": true,
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Nenhum registro encontrado",
                "info": "Exibindo página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhuma informação preenchida",
                "infoFiltered": "(filtrados de _MAX_ total registros)"
            },
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tudo"]],
            "searching": true,
            "dom": '<"top"ft>rt<"bottom"il><"clear"p>'

        });
    });
</script>