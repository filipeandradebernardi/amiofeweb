<h3 class="page-title"><?= $title ?> </h3>
    <?php $this->user = $this->session->userdata('user_amiofe'); ?>
<section class="panel">
    <div class="panel-body">
        <a href="<?= $this->config->base_url('home/new_user') ?>" class="btn btn-success btn-lg pull-left"><i class="fa fa-plus-circle"></i> Adicionar</a>

        <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" data-swf-path="<?= $this->config->base_url(ASSETSPATH . 'jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf') ?>">
            <thead>
                <tr>
                    <th>Usuário</th>
                    <th>Email</th>
                    <th>Permissão</th>
                    <?php if ($this->user['permission'] == 'su'): ?>
                        <th></th>
                    <?php endif; ?>
                </tr>
            </thead>
            <tbody>
                <?php if (isset($users)): ?>
                    <?php foreach ($users as $p): ?>
                        <tr class="content_row">
                            <td><?= $p['name'] ?></td>
                            <td><?= $p['email'] ?></td>
                            <td><?= lang($p['permission']) ?></td>
                            <?php if ($this->user['permission'] == 'su'): ?>
                                <td>
                                    <a class=" btn default" title="Editar usuário" href="<?= $this->config->base_url('home/edit_user') . '/' . $p['id'] ?>" data-target="#ajax" data-toggle="modal"><i class="fa fa-pencil"></i></a>
                                    <a href="javascript:void(0)" title="Deletar usuário" onclick="delete_user(<?= $p['id'] ?>)"><i class="fa fa-trash-o"></i></a>
                                </td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</section>