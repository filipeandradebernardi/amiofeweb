<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8"/>
        <title>Amiofe</title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/uniform/css/uniform.default.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/css/login.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/css/components.css') ?>" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'global/css/plugins.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/css/layout.css') ?>" rel="stylesheet" type="text/css"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/css/themes/default.css') ?>" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?= $this->config->base_url(ASSETSPATH . 'admin/layout2/css/custom.css') ?>" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="stylesheet" href="<?= $this->config->base_url(ASSETSPATH . 'pnotify/pnotify.custom.css') ?> " />


    </head>

    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login" data-baseurl="<?= $this->config->base_url(); ?>">
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->

        <!-- END SIDEBAR TOGGLER BUTTON -->
        <!-- BEGIN LOGO -->
        <!--        <div class="logo">
                    <a href="index.html">
                        <img src="<?= $this->config->base_url(ASSETSPATH . "admin/layout2/img/logo-big.png") ?>" alt=""/>
                    </a>
                </div>-->
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->

        <div class="menu-toggler sidebar-toggler">
        </div>
        <div class="content">

            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="index.html" method="post">
                <h3 class="form-title">Bem vindo</h3>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Usuário</label>
                    <input onkeyup="login_enter(event);" class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Usuário" id="username"/>
                </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Senha</label>
                    <input onkeyup="login_enter(event);" class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Senha" id="password"/>
                </div>
                <div class="form-actions">
                    <a href="javascript:void(0)" onclick="login()" class="btn btn-success uppercase">Entrar</a>

                    <label class="rememberme check">
                        <input type="checkbox" id="RememberMe"/>Salvar minha senha
                    </label>
                </div>
            </form>
            <!-- END LOGIN FORM -->

            <div id="register" class="modal fade" tabindex="-1" aria-hidden="true">
                <form role="form">
                    <div class="form-body">
                        <div class="form-group email">
                            <label>Email*</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope"></i>
                                </span>
                                <input type="text" id="email" class="form-control input-lg" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-group password">
                            <label for="exampleInputPassword1">Senha*</label>
                            <div class="input-group">
                                <input type="password" class="form-control input-lg" id="password" placeholder="Senha">
                                <span class="input-group-addon">
                                    <i class="fa fa-key"></i>
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 name">
                                <label>Nome*</label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-user"></i>
                                    <input type="text" id="name" class="form-control input-lg" placeholder="Nome">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Permissão</label>
                                <select class="form-control input-lg" id="permission">
                                    <option value="default">Usuário comum</option>
                                    <option value="su">Super usuário</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Cidade*</label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-user"></i>
                                    <input type="text" id="city" class="form-control input-lg" placeholder="Cidade">
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Unidade</label>
                                <select class="form-control input-lg" id="institution">
                                    <option value="usp">USP</option>
                                    <option value="other">Outra</option>
                                </select>
                            </div>


                        </div>

                    </div>
                    <div class="form-actions">
                        <a href="javascript:void(0)" onclick="save_user()" class="btn blue">Salvar</a>
                    </div>
                </form>
            </div>

        </div>

        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery-migrate.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/plugins/jquery.cokie.min.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'global/scripts/metronic.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/layout/scripts/layout.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/layout/scripts/demo.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/scripts/login.js') ?>" type="text/javascript"></script>
        <script src="<?= $this->config->base_url(JSPATH . 'login.js') ?>"></script>




        <script>
                            jQuery(document).ready(function () {
                                Metronic.init(); // init metronic core components
                                Layout.init(); // init current layout
                                Demo.init(); // init demo features 
                            });
        </script>
        <script src="<?= $this->config->base_url(ASSETSPATH . 'pnotify/pnotify.custom.js') ?>"></script>   
    </body>

    <!-- END BODY -->
</html>