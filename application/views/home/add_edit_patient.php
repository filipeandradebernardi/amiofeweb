<script src="<?= $this->config->base_url(JSPATH . 'jquery.mask.min.js') ?>" type="text/javascript"></script>

<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus-circle"></i> Cadastrando novo Indivíduo
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form">
            <div class="form-body">
                <div class="row">
                    <div class="form-group col-md-6 name">
                        <label>Nome Completo*</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-child"></i>
                            <input type="text" id="name" class="form-control input-lg" placeholder="Nome Completo">
                        </div>
                    </div>

                    <div class="form-group col-md-6">
                        <label>Nome do responsável</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-user"></i>
                            <input type="text" id="mother_name" class="form-control input-lg" placeholder="Nome do responsável">
                        </div>                       
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label>CPF</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-bars"></i>
                            <input type="text" id="cpf" class="form-control input-lg" placeholder="CPF">
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Nº do cartão SUS</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-barcode"></i>
                            <input type="text" id="sus_number" class="form-control input-lg" placeholder="Nº SUS">
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Sexo</label>
                        <select class="form-control input-lg" id="gender">
                            <option value="male">Masculino</option>
                            <option value="female">Feminino</option>
                        </select>
                    </div>
                    <div class="form-group col-md-3 birthdate">
                        <label>Data de nascimento*</label>
                        <div class="input-group date " data-date-format="yyyy-mm-dd">
                            <input type="date" class='form-control input-lg' id="birthdate">
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Idade</label>
                        <div class="input-group">
                            <input type="number" class='form-control input-lg'readonly id="age">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 telephone">
                        <label for="txttelefone">Telefone*</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-phone"></i>
                            <input type="tel" class="form-control input-lg" placeholder="Telefone" name="txttelefone" id="txttelefone" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}" />
                            <script type="text/javascript">$("#txttelefone").mask("(00) 0000-0009");</script>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="cellphone">Celular</label>
                        <div class="input-icon input-icon-lg">
                            <i class="icon-screen-smartphone"></i>
                            <input type="tel" class="form-control input-lg" placeholder="Celular" name="cellphone" id="cellphone" pattern="\([0-9]{2}\)[\s][0-9]{4}-[0-9]{4,5}" />
                            <script type="text/javascript">$("#cellphone").mask("(00) 0000-00009");</script>
                        </div>
                    </div>

                </div>
                <div class="row">

                    <div class="form-group col-md-2">
                        <label>CEP</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-map-marker"></i>
                            <input type="text" id="cep" class="form-control input-lg" placeholder="CEP">
                        </div>
                    </div>
                    <div class="form-group col-md-3 city">
                        <label>Cidade*</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-building"></i>
                            <input type="text" id="city" class="form-control input-lg" placeholder="Cidade">
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label>Estado*</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-building-o"></i>
                            <input type="text" id="state" class="form-control input-lg" placeholder="Estado">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-4 street">
                        <label>Rua*</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-road"></i>
                            <input type="text" id="street" class="form-control input-lg" placeholder="Rua">
                        </div>                       
                    </div>
                    <div class="form-group col-md-3">
                        <label>Bairro</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-map-marker"></i>
                            <input type="text" id="neighborhood" class="form-control input-lg" placeholder="Bairro">
                        </div>                       
                    </div>

                    <div class="form-group col-md-2 number">
                        <label>Número*</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-sort-numeric-asc"></i>
                            <input type="number" id="number" class="form-control input-lg" placeholder="Número">
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        <label>Complemento</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-pencil-square-o"></i>
                            <input type="text" id="complement" class="form-control input-lg" placeholder="Complemento">
                        </div>                       
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <a href="javascript:void(0)" onclick="save_patient()" class="btn blue">Salvar</a>
            </div>
        </form>
    </div>
</div>
<script src="<?= $this->config->base_url(ASSETSPATH . 'admin/pages/scripts/components-pickers.js') ?>"></script>
<script src="<?= $this->config->base_url(JSPATH . 'bootstrapAlert.js') ?>" type="text/javascript"></script>
<script src="<?= $this->config->base_url(JSPATH . 'bootstrapAlert.min.js') ?>" type="text/javascript"></script>

<script>
                    jQuery(document).ready(function () {
                        ComponentsPickers.init();
                    });
                    function save_patient() {
                        if (!jQuery("#name").val()) {
                            jQuery('.name').addClass('has-error');
                            return false;
                        }
                        if (!jQuery("#birthdate").val()) {
                            jQuery('.birthdate').addClass('has-error');
                            alertafalha('O campo Data de Nascimento é obrigatório');
                            return false;
                        }
                        if (!jQuery("#txttelefone").val()) {
                            jQuery('.telephone').addClass('has-error');
                            return false;
                        }
                        if (!jQuery("#city").val()) {
                            jQuery('.city').addClass('has-error');
                            return false;
                        }
                        if (!jQuery("#street").val()) {
                            jQuery('.street').addClass('has-error');
                            return false;
                        }
                        if (!jQuery("#number").val()) {
                            jQuery('.number').addClass('has-error');
                            return false;
                        }


                        jQuery.ajax({
                            url: jQuery("body").data("baseurl") + "home/save_patient",
                            type: "post",
                            dataType: 'json',
                            data: {
                                name: jQuery("#name").val(),
                                cpf: jQuery("#cpf").val(),
                                sus_number: jQuery("#sus_number").val(),
                                mother_name: jQuery("#mother_name").val(),
                                telephone: jQuery("#txttelefone").val(),
                                cellphone: jQuery("#cellphone").val(),
                                city: jQuery("#city").val(),
                                state: jQuery("#state").val(),
                                street: jQuery("#street").val(),
                                neighborhood: jQuery("#neighborhood").val(),
                                age: jQuery("#age").val(),
                                number: jQuery("#number").val(),
                                cep: jQuery("#cep").val(),
                                complement: jQuery("#complement").val(),
                                gender: jQuery("#gender").val(),
                                birthdate: jQuery("#birthdate").val()
                            },
                            success: function (response) {
                                if (response.status == "OK") {
                                    setTimeout(alertasucesso(), 1500);
                                    window.location = jQuery("body").data('baseurl') + 'home/new_test';
                                }
                            }
                        });

                    }
</script>
<script>
    document.getElementById("birthdate").addEventListener('change', function () {
        var data = new Date(this.value);
        if (isDate_(this.value) && data.getFullYear() > 1900)
            document.getElementById("age").value = calculateAge(this.value);
    });



    function alertasucesso() {
        BootstrapAlert.success({
            title: "Indivíduo inserido com sucesso!",
            message: "OK"
        });
    }

    function alertafalha(msg) {
        BootstrapAlert.alert({
            title: "Atenção!",
            message: msg
        });
    }

    function calculateAge(dobString) {
        var dob = new Date(dobString);
        var currentDate = new Date();
        var currentYear = currentDate.getFullYear();
        var birthdayThisYear = new Date(currentYear, dob.getMonth(), dob.getDate());
        var age = currentYear - dob.getFullYear();
        if (birthdayThisYear > currentDate) {
            age--;
        }
        return age;
    }

    function calcular(data) {
        var data = document.form.nascimento.value;
        alert(data);
        var partes = data.split("/");
        var junta = partes[2] + "-" + partes[1] + "-" + partes[0];
        document.form.idade.value = (calculateAge(junta));
    }

    var isDate_ = function (input) {
        var status = false;
        if (!input || input.length <= 0) {
            status = false;
        } else {
            var result = new Date(input);
            if (result == 'Invalid Date') {
                status = false;
            } else {
                status = true;
            }
        }
        return status;
    }
</script>  
<script type="text/javascript">
    $("#cep").focusout(function () {
        //Início do Comando AJAX
        $.ajax({
            //O campo URL diz o caminho de onde virá os dados
            //É importante concatenar o valor digitado no CEP
            url: 'https://viacep.com.br/ws/' + $(this).val() + '/json/unicode/',
            //Aqui você deve preencher o tipo de dados que será lido,
            //no caso, estamos lendo JSON.
            dataType: 'json',
            //SUCESS é referente a função que será executada caso
            //ele consiga ler a fonte de dados com sucesso.
            //O parâmetro dentro da função se refere ao nome da variável
            //que você vai dar para ler esse objeto.
            success: function (resposta) {
                //Agora basta definir os valores que você deseja preencher
                //automaticamente nos campos acima.
                $("#street").val(resposta.logradouro);
                $("#complement").val(resposta.complemento);
                $("#neighborhood").val(resposta.bairro);
                $("#city").val(resposta.localidade);
                $("#state").val(resposta.uf);
                //Vamos incluir para que o Número seja focado automaticamente
                //melhorando a experiência do usuário
                $("#number").focus();
            }
        });
    });
</script>