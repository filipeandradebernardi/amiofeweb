<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus-circle"></i> Classificar teste
        </div>
    </div>


    <section class="panel">
        <div class="panel-body">

            <div class="portlet-body form">
                <form role="form">
                    <div class="form-body">
                        <div class="row">

                            <div class="form-group col-md-12">
                                <label>Categoria do Grupo:</label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-group"></i>
                                    <select class="form-control input-lg" id="category_group">
                                        <option value="vazio"></option>
                                        <option value="Controle">1 - Controle</option>
                                        <option value="Não Controle">2 - Não Controle</option>
                                        <option value="Excluidos">3 - Excluidos</option>
                                    </select>
                                </div>
                            </div>
                        </div>        
                        <div class="row">

                            <div class="form-group col-md-12">
                                <label>Subgrupo:</label>
                                <div class="input-icon input-icon-lg">
                                    <i class="fa fa-group"></i>
                                    <select class="form-control input-lg" id="subgroup_name">
                                    </select>
                                </div>
                            </div>
                        </div>   

                        <div class="row">

                            <div class="form-group col-md-12">
                                <div class="input-icon input-icon-lg">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-tabletools" >
                                        <tr>
                                            <?php if (isset($patient_group[0])): ?>
                                                <th>ID do Teste </th>
                                                <th>Paciente</th>
                                                <th>Data do exame</th>
                                                <th>Responsável</th>
                                                <th>Escore Total</th>
                                                <th>Grupo</th>
                                                <th>Subgrupo</th>

                                            <?php endif; ?>

                                        </tr>
                                        </thead>

                                        <tbody>
                                            <?php if (isset($patient_group)): ?>
                                                <?php foreach ($patient_group as $p): ?>

                                                    <tr class="content_row">
                                                        <td><?= $p['id'] ?></td>
                                                        <td><?= $this->encrypt->decode($p['patient_name']) ?></td>
                                                        <td><?= $p['realization_date'] ?></td>
                                                        <td><?= $p['user_name'] ?></td>
                                                        <td><?= $p['tab1_resultgeral'] ?></td>
                                                        
                                                        <td><?= $p['group'] ?></td>
                                                        <td>
                                                            <?php foreach ($p['patient_subgroups'] as $s): ?>
                                                                <?= $s['subgroup_name'] ?><br> 
                                                            <?php endforeach; ?>
                                                        </td>
                                                        <td>
                                                            <div id='checkboxes'>
                                                                <label>
                                                                    <input id="<?= $p['id'] ?>" type="checkbox">
                                                                </label> </div>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>    
                        <a href="javascript:void(0)" onclick="add_test_to_subgroup()" class="btn btn-success"><i class="fa fa-plus-circle"></i> Adicionar testes ao Subgrupo</a>

                    </div>

                    <div class="form-actions">
                    </div>
                </form>

            </div>

        </div>

    </section>
</div>

<script>
    jQuery(document).ready(function () {
        ComponentsPickers.init();
    });
    jQuery('#category_group').on('change', function () {
        get_subgroups();
    });

    function get_subgroups() {

        if (!jQuery("#category_group").val()) {
            jQuery('#category_group').addClass('has-error');
            return false;
        }
        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "home/get_subgroups",
            type: "post",
            dataType: 'json',
            data: {
                category_group: jQuery("#category_group").val()

            },
            success: function (response) {

                if (response.status == "200") {
                    jQuery('#subgroup_name').html('');
                    // window.location = jQuery("body").data('baseurl') + 'home/groups';
                    $.each(response.subgroups, function (key, value) {
                        jQuery('#subgroup_name').append('<option value=' + value.id + '>' + value.subgroup_name + '</option>');
                    });
                } else {

                    jQuery('#subgroup_name').empty();
                }
            }
        });

    }
</script>

<script>
    jQuery(document).ready(function () {
        ComponentsPickers.init();
    });


    function add_test_to_subgroup() {

        var selected = [];
        $('div#checkboxes input[type=checkbox]').each(function () {
            if ($(this).is(":checked")) {
                selected.push($(this).attr('id'));
            }
        });

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "home/update_subgroup",
            type: "post",
            dataType: 'json',
            data: {
                category_group: jQuery("#category_group").val(),
                subgroup_name: jQuery("#subgroup_name option:selected").text(), 
                id_test: selected

            },
            success: function (response) {

                if (response.status == "OK") {
                    window.location = jQuery("body").data('baseurl') + 'home/groups';
                }
            }
        });

    }
</script>