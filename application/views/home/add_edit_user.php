<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus-circle"></i> Adicionando novo usuário
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form">
            <div class="form-body">
                <div class="form-group email">
                    <label>Email*</label>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="fa fa-envelope"></i>
                        </span>
                        <input type="text" id="email" class="form-control input-lg" placeholder="Email">
                    </div>
                </div>
                <div class="form-group password">
                    <label for="exampleInputPassword1">Senha*</label>
                    <div class="input-group">
                        <input type="password" class="form-control input-lg" id="password" placeholder="Senha">
                        <span class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </span>
                    </div>
                </div>
                <div class="form-group password_conf">
                    <label for="exampleInputPassword1">Confirmação de senha*</label>
                    <div class="input-group">
                        <input type="password" class="form-control input-lg" id="password_confirmation" placeholder="Confirmação de senha">
                        <span class="input-group-addon">
                            <i class="fa fa-key"></i>
                        </span>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 name">
                        <label>Nome*</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-user"></i>
                            <input type="text" id="name" class="form-control input-lg" placeholder="Nome">
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Permissão</label>
                        <select class="form-control input-lg" id="permission">
                            <option value="default">Usuário comum</option>
                            <option value="su">Super usuário</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>Cidade*</label>
                        <div class="input-icon input-icon-lg">
                            <i class="fa fa-user"></i>
                            <input type="text" id="city" class="form-control input-lg" placeholder="Cidade">
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Unidade</label>
                        <select class="form-control input-lg" id="institution">
                            <option value="usp">USP</option>
                            <option value="other">Outra</option>
                        </select>
                    </div>


                </div>

            </div>
            <div class="form-actions">
                <a href="javascript:void(0)" onclick="save_user()" class="btn blue">Salvar</a>
            </div>
        </form>
    </div>
</div>

<script>






    function save_user() {

        if (!jQuery("#name").val()) {
            jQuery('.name').addClass('has-error');
            return false;
        }
        if (!jQuery("#email").val()) {
            jQuery('.email').addClass('has-error');
            return false;
        }
        if (!jQuery("#password").val()) {
            jQuery('.password').addClass('has-error');
            return false;
        }
        if (!jQuery("#password_confirmation").val()) {
            jQuery('.password_conf').addClass('has-error');
            return false;
        }

        if (jQuery('#password').val() !== jQuery('#password_confirmation').val()) {
            var notice = new PNotify({
                title: 'Nome',
                text: 'Senha e confirmação de senha estão diferentes',
                type: 'error',
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
            return false;
        } 

        jQuery.ajax({
            url: jQuery("body").data("baseurl") + "home/save_user",
            type: "post",
            dataType: 'json',
            data: {
                name: jQuery("#name").val(),
                email: jQuery("#email").val(),
                password: jQuery("#password").val(),
                institution: jQuery("#institution").val(),
                permission: jQuery("#permission").val()
            },
            success: function (response) {
                if (response.status == "OK") {
                    window.location = jQuery("body").data('baseurl') + 'home/list_users';
                }
            }
        });
    }
</script>