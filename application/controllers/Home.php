<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->user = $this->session->userdata('user_amiofe');
        $this->load->model('User_Model');
        $this->load->model('Home_Model');
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function index() {
        if ($this->user['logged']) {
            $data['title'] = 'Novo teste';
            $data['patients'] = $this->Home_Model->get_patients();
            $data['content'] = 'home/index';
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    // FUNCOES PACIENTE
    // pega pacientes no banco e mostra na tela
    function list_patients() {
        if ($this->user['logged']) {
            $data['title'] = 'Indivíduos';
            $data['patients'] = $this->Home_Model->get_patients();
            $data['content'] = 'home/list_patients';

            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    // salva informacoes editadas do paciente no banco
    function update_patient() {
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        $status = $this->Home_Model->update_patient($data, $id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    // abrindo pagina para adicionar paciente
    function new_patient() {
        if ($this->user['logged']) {
            $data['title'] = 'Indivíduos';
            $data['content'] = 'home/add_edit_patient';
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    function new_subgroup() {
        if ($this->user['logged']) {
            $data['title'] = 'Sub Grupo';
            $data['content'] = 'home/add_edit_subgroup';
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    function patient_group() {
        if ($this->user['logged']) {
            $data['title'] = 'Classificar Teste';
            $data['content'] = 'home/patient_group';
            $data['patient_group'] = $this->Home_Model->get_patientgroup();

            foreach ($data['patient_group'] as $key => $p) {
                $data['patient_group'][$key]['patient_subgroups'] = $this->Home_Model->get_patientsubgroup($p['id']);
            }
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    function get_subgroups() {
        $cat = $this->input->post('category_group');
        $groups = $this->Home_Model->get_subgroups($cat);
        if (empty($groups)) {
            $status = "500";
        } else {
            $status = "200";
        }
        echo json_encode(array(
            'status' => $status,
            'subgroups' => $groups
        ));
        exit;
    }

    function delete_subgroup() {
        $id = $this->input->post('id');
        $status = $this->Home_Model->delete_subgroup($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function update_subgroup() {
        $data = $this->input->post();

        foreach ($data['id_test'] as $k) {
            $data2['category_group'] = $data['category_group'];
            $data2['subgroup_name'] = $data['subgroup_name'];
            $data2['name_user'] = $this->user['name'];
            $data2['id_user'] = $this->user['id'];
            $data2['id_test'] = $k;
            $updatetest['group'] = $data['category_group'];
            $updatetest['id'] = $k;
        }
        // print_r('<pre');
        // print_r($data2);

        $this->Home_Model->update_test_group($updatetest, $updatetest['id']);
        $status = $this->Home_Model->update_subgroup($data2);
        echo json_encode(array(
            'status' => $status
        ));
        exit;

        //print('<pre>');
        //print_r($data);exit;
    }

    public function save_register() {
        $data = $this->input->post();
        //checa se nao existe email igual no banco
        if ($this->User_Model->user_already_exists($data['email'])) {
            //retorna erro
            $status = 'NOK';
            $message = 'Email ja cadastrado, tente outro';
            $title = 'Erro';
        } else {
            $data['permission'] = 'default';

            $user_id = $this->User_Model->create_user($data);
            if ($user_id != 'NOK') {
                // cadatro feito com sucesso, carrega session
                $user = $this->User_Model->get_user($user_id);

                $session = array(
                    'user_amiofe' => array(
                        'logged' => TRUE,
                        'id' => $user['id'],
                        'name' => $user['name'],
                        'permission' => $user['permission']
                    )
                );
                $this->session->set_userdata($session);

                $status = 'OK';
                $message = '';
                $title = '';
            } else {
                // algum erro aconteceu
                $status = 'NOK';
                $message = 'Tentar novamente mais tarde';
                $title = 'Erro';
            }
        }
        echo json_encode(array(
            'status' => $status,
            'message' => $message,
            'title' => $title
        ));
        exit;
    }

    function save_subgroup() {
        $data = $this->input->post();
        $data['id_user'] = $this->user['id'];
        $data['name_user'] = $this->user['name'];
        $status = $this->Home_Model->save_subgroup($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    //modal para editar paciente
    function edit_patient($id) {
        $data['patient'] = $this->Home_Model->get_patient($id);
        $this->load->view('home/edit_patient_modal', $data);
    }

    // salvando paciente no banco
    function save_patient() {
        $data = $this->input->post();
////        $data['id'] = $this->encrypt->encode($data['id']);
//        $data['name'] = $this->encrypt->encode($data['name']);
//        //$data['birthdate'] = $this->encrypt->encode($data['birthdate']);
//        $data['city'] = $this->encrypt->encode($data['city']);
//        $data['street'] = $this->encrypt->encode($data['street']);
//        $data['number'] = $this->encrypt->encode($data['number']);
//        $data['telephone'] = $this->encrypt->encode($data['telephone']);
//        $data['cellphone'] = $this->encrypt->encode($data['cellphone']);
//        $data['mother_name'] = $this->encrypt->encode($data['mother_name']);
//        $data['gender'] = $this->encrypt->encode($data['gender']);
//        $data['complement'] = $this->encrypt->encode($data['complement']);
//        print_r('<pre>'); print_r($data);exit;
        $status = $this->Home_Model->save_patient($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    // inativando paciente do banco
    function delete_patient() {
        $id = $this->input->post('id');
        $status = $this->Home_Model->delete_patient($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    // FUNCOES USUARIO
    // pega usuarios no banco e mostra na tela
    function list_users() {
        if ($this->user['logged']) {
            $data['title'] = 'Usuários';
            $data['users'] = $this->Home_Model->get_users();
            $data['content'] = 'home/list_users';
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    // salvando usuario no banco
    function save_user() {
        $data = $this->input->post();
        $status = $this->Home_Model->save_user($data);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    // salva informacoes editadas do usuario no banco
    function update_user() {
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        if ($this->user['permission'] != 'su') {
            unset($data['permission']);
        }
        $status = $this->Home_Model->update_user($data, $id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    // abrindo pagina para adicionar usuario
    function new_user() {
        if ($this->user['logged']) {
            $data['title'] = 'Usuários';
            $data['content'] = 'home/add_edit_user';
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    //modal para editar usuario
    function edit_user($id) {
        $data['user'] = $this->User_Model->get_user($id);
        $this->load->view('home/edit_user_modal', $data);
    }

    // inativando usuario do banco
    function delete_user() {
        $id = $this->input->post('id');
        $status = $this->Home_Model->delete_user($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    // FUNCOES TESTES
    // pega testes no banco e mostra na tela
    function list_tests() {
        if ($this->user['logged']) {
            $data['title'] = 'Avaliações';
            $data['tests'] = $this->Home_Model->get_tests();
            // print_r('<pre>'); print_r($data);exit;exit;

            foreach ($data['tests'] as $key => $test) {
                //unset($test['id']);
                // unset($test['patient_id']);
                //unset($test['status']);
                //unset($test['video_path']);
                $data['tests'][$key] = $test;
            }

            //  print_r('<pre>'); print_r($data);exit;exit;
            $data['content'] = 'home/list_tests';
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    // abrindo pagina para adicionar teste
    function new_test() {
        if ($this->user['logged']) {
            $data['teste'] = null;
            $data['title'] = 'Testes';
            $data['patients'] = $this->Home_Model->get_patients();
            $data['content'] = 'home/new_test';
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    function new_test_patient($patient_id) {
        if ($this->user['logged']) {
            $data['teste'] = null;
            $data['title'] = 'Avaliação';
            $data['patients'] = $patient_id;
            $data['content'] = 'home/new_test';
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    // salvando teste no banco
    function save_test() {
        $data = $this->input->post();
        $data['user_id'] = $this->user['id'];

        foreach ($data as $key => $value) {
            if ($value == 'true') {
                $data[$key] = 1;
            } else if ($value == 'false') {
                $data[$key] = 0;
            }
        }
//        print_r("<pre>");
//        print_r($data);
//        exit;


        $status = $this->Home_Model->save_test($data);

        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    // inativando teste do banco
    function delete_test() {
        $id = $this->input->post('id');
        $status = $this->Home_Model->delete_test($id);
        echo json_encode(array(
            'status' => $status
        ));
        exit;
    }

    function view_test($id) {
        $data['teste'] = $this->Home_Model->get_test($id);

        // setando valores NULL para 9(que nao significa nada no sistema)
//        if ($data['teste']['radiof4'] === NULL) {
//            $data['teste']['radiof4'] = 9;
//        } else if ($data['teste']['radiof4'] === '0') {
//            $data['teste']['radiof4'] = 0;
//        }
//        if ($data['teste']['radiof5'] === NULL) {
//            $data['teste']['radiof5'] = 9;
//        } else if ($data['teste']['radiof5'] === '0') {
//            $data['teste']['radiof5'] = 0;
//        }
//        if ($data['teste']['radiof6'] === NULL) {
//            $data['teste']['radiof6'] = 9;
//        } else if ($data['teste']['radiof6'] === '0') {
//            $data['teste']['radiof6'] = 0;
//        }
//        if ($data['teste']['radiof11'] === NULL) {
//            $data['teste']['radiof11'] = 9;
//        } else if ($data['teste']['radiof11'] === '0') {
//            $data['teste']['radiof11'] = 0;
//        }
//        if ($data['teste']['radiof12'] === NULL) {
//            $data['teste']['radiof12'] = 9;
//        } else if ($data['teste']['radiof12'] === '0') {
//            $data['teste']['radiof12'] = 0;
//        }
//        if ($data['teste']['radiof13'] === NULL) {
//            $data['teste']['radiof13'] = 9;
//        } else if ($data['teste']['radiof13'] === '0') {
//            $data['teste']['radiof13'] = 0;
//        }

//        print_r('<pre>'); print_r($data);exit;
        $data['title'] = 'Visualizar teste';;
        $data['content'] = 'home/new_test';
        $this->load->view('layouts/logged', $data);
    }

    function edit_test($id) {
        $data['teste'] = $this->Home_Model->get_test($id);

        // setando valores NULL para 9(que nao significa nada no sistema)
        if ($data['teste']['radiof4'] === NULL) {
            $data['teste']['radiof4'] = 9;
        } else if ($data['teste']['radiof4'] === '0') {
            $data['teste']['radiof4'] = 0;
        }
        if ($data['teste']['radiof5'] === NULL) {
            $data['teste']['radiof5'] = 9;
        } else if ($data['teste']['radiof5'] === '0') {
            $data['teste']['radiof5'] = 0;
        }
        if ($data['teste']['radiof6'] === NULL) {
            $data['teste']['radiof6'] = 9;
        } else if ($data['teste']['radiof6'] === '0') {
            $data['teste']['radiof6'] = 0;
        }
        if ($data['teste']['radiof11'] === NULL) {
            $data['teste']['radiof11'] = 9;
        } else if ($data['teste']['radiof11'] === '0') {
            $data['teste']['radiof11'] = 0;
        }
        if ($data['teste']['radiof12'] === NULL) {
            $data['teste']['radiof12'] = 9;
        } else if ($data['teste']['radiof12'] === '0') {
            $data['teste']['radiof12'] = 0;
        }
        if ($data['teste']['radiof13'] === NULL) {
            $data['teste']['radiof13'] = 9;
        } else if ($data['teste']['radiof13'] === '0') {
            $data['teste']['radiof13'] = 0;
        }

//        print_r('<pre>'); print_r($data);exit;
        $data['title'] = 'Editar teste';
        $data['content'] = 'home/new_test';
        $this->load->view('layouts/logged', $data);
    }

    function show_graph($id) {
        $data['teste'] = $this->Home_Model->get_test($id);

        //print_r("<pre>");
        //print_r($data);
        //exit;
        // TOTAL
        $title_total = array('Aparência e condição postural/posição', 'Mobilidade', 'Funções', 'Total');
        $value_referencia = array(18, 57, 29, 104);
        $value_total = array($data['teste']['tab1_result'], $data['teste']['tab2_result'], $data['teste']['tab3_result'], $data['teste']['tab1_resultgeral']);
        $data['title_total'] = json_encode($title_total);
        $data['value_total'] = json_encode($value_total, JSON_NUMERIC_CHECK);
        $data['value_referencia'] = json_encode($value_referencia, JSON_NUMERIC_CHECK);

        // APARENCIA E CONDIÇÃO POSTURAL - GERAL   
        $title_tab1 = array('Normal', 'Lábios', 'Mandíbula', 'Bochechas', 'Face', 'Língua', 'Aparência do palato duro');
        $value_tab1 = array(0, $data['teste']['radioa1'], $data['teste']['radioa2'], $data['teste']['radioa3'], $data['teste']['radioa4'], $data['teste']['radioa5'], $data['teste']['radioa6']);
        $data['title_tab1'] = json_encode($title_tab1);
        $data['value_tab1'] = json_encode($value_tab1, JSON_NUMERIC_CHECK);

        //MOBILIDADE - GERAL
        $title_tab2 = array('Labiais', 'Língua', 'Mandíbula', 'Bochecha');
        $value_referencia_mob = array(12, 18, 15, 12);
        $value_tab2 = array($data['teste']['tab2_1_result'], $data['teste']['tab2_2_result'], $data['teste']['tab2_3_result'], $data['teste']['tab2_4_result']);
        $data['title_tab2'] = json_encode($title_tab2);
        $data['value_tab2'] = json_encode($value_tab2, JSON_NUMERIC_CHECK);
        $data['value_referencia_mob'] = json_encode($value_referencia_mob, JSON_NUMERIC_CHECK);

        //FUNÇÕES - GERAL
        $title_tab3 = array('Respiração', 'Deglutição', 'Mastigação');
        $value_tab3 = array($data['teste']['tab3_1_result'], $data['teste']['tab3_2_result'], $data['teste']['tab3_3_result']);
        $data['title_tab3'] = json_encode($title_tab3);
//        $data['value_tab3'] = json_encode($value_tab3, JSON_NUMERIC_CHECK);
        //MOBILIDADE -ESPECÍFICOS        
//        $title_tab2_movimentos = array('Desempenho', 'Preciso', 'Falta de precisão/tremor', 'Inabilidade severa');
//        $title_tab2_movimentos = json_encode($title_tab2_movimentos);




        $data['title'] = 'Gráfico';
        $data['content'] = 'home/show_graph';
        $this->load->view('layouts/logged', $data);
    }

    function receive_video() {

        $test_id = $this->input->post('test_id');

        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $targetFile = 'public/videos/' . $_FILES['file']['name'];
            $additional = '1';
            while (file_exists($targetFile)) {
                $info = pathinfo($targetFile);
                $targetFile = $info['dirname'] . '/'
                        . $info['filename'] . $additional
                        . '.' . $info['extension'];
            }
            $return = move_uploaded_file($tempFile, $targetFile);
            // salvando caminho do video no banco
            $status = $this->Home_Model->save_video_test($test_id, $targetFile);
        }
    }

    function get_video_test($test_id) {
        $test = $this->Home_Model->get_video_test($test_id);
        $this->load->helper('file');
        $extension = 'video/mp4';
        header("Cache-Control: maxage=1");
        header("Pragma: public");
        header("Content-type: " . $extension);
        header('Content-Length:' . filesize($test['video_path']));
        try {
            while (@ob_end_flush());
        } catch (Exception $e) {
            
        }
        readfile($test['video_path']);
    }

    function groups() {
        if ($this->user['logged']) {
            $data['groups'] = $this->Home_Model->get_groups();
            $data['title'] = 'Grupos';
            $data['content'] = 'home/groups';
            $this->load->view('layouts/logged', $data);
        } else {
            $this->load->view('home/login');
        }
    }

    function export_complete_test($test_id) {
        $test = $this->Home_Model->export_complete_test($test_id);
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('TESTE');
        // load database
        $this->load->database();
        //load model
        //$this->load->model('userModel');
        // get all users in array formate
        //$users = $this->userModel->get_users();
        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($test);

        $filename = 'batata.xls'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format
        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');


//        $file = $this->config->base_url(EXCELPATH .'template.xlsx');
//        //load the excel library
//        $this->load->library('excel');
//        //read file from path
//        $objPHPExcel = PHPExcel_IOFactory::load($file);
//        //get only the Cell Collection
//        $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
//
//        foreach ($cell_collection as $cell) {
//            $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
//            $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
//            $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
//        }
//        if ($row == 1) {
//            $header[$row][$column] = $data_value;
//        } else {
//            $arr_data[$row][$column] = $data_value;
//        }
//        $data['header'] = $header;
//        $data['values'] = $arr_data;
    }

    function teste_excel($id) {
        //$file = EXCELPATH . 'template.xlsx';
        //carrega  biblioteca
        //load our new PHPExcel library
        $this->load->library('excel');
        //activate worksheet number 1
        $this->excel->setActiveSheetIndex(0);
        //name the worksheet
        $this->excel->getActiveSheet()->setTitle('Users list');

        // load database
        $this->load->database();

        // load model
        $this->load->model('Home_model');

        // get all users in array formate
        $users = $this->Home_model->export_complete_test($id);

        // read data to active sheet
        $this->excel->getActiveSheet()->fromArray($users);

        $filename = 'just_some_random_name.xls'; //save our workbook as this file name

        header('Content-Type: application/vnd.ms-excel'); //mime type

        header('Content-Disposition: attachment;filename="' . $filename . '"'); //tell browser what's the file name

        header('Cache-Control: max-age=0'); //no cache
        //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
        //if you want to save it as .XLSX Excel 2007 format

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        //force user to download the Excel file without writing it to server's HD
        $objWriter->save('php://output');
    }

}
