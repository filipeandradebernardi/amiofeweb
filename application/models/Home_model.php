<?php

Class Home_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function create_user($user) {

        $this->db->trans_start();

        $this->db->set('name', $user['name'])
                ->set('email', $user['email'])
                ->set('password', $user['password'])
                ->set('permission', $user['permission'])
                ->insert('users');
        $id = $this->db->insert_id();

        $this->db->trans_complete();

        if ($id) {
            return $id;
        } else {
            return "NOK";
        }
    }

    /**
     * verifica se existe um usuario com name ou email
     * @param type $user
     */
    public function user_already_exists($email) {

        $query_email = $this->db->select('id')
                ->from('users')
                ->where('email', $email)
                ->get();

        if ($query_email->num_rows > 0) {
            return true;
        }
        return false;
    }

    function save_video_test($test_id, $video_path) {
        $query = $this->db->set('video_path', $video_path)
                ->where('id', $test_id)
                ->update('tests');
        if ($query) {
            return "OK";
        } else {
            return "NOK";
        }
    }

    function get_video_test($test_id) {
        return $this->db->select('*')
                        ->from('tests')
                        ->where('id', $test_id)
                        ->get()->row_array();
    }

    function get_test($test_id) {
        return $this->db->select('t.id,t.patient_id,t.*,p.name')
                        ->from('tests t')
                        ->join('patients p', 'p.id=t.patient_id')
                        ->where('t.id', $test_id)
                        ->get()->row_array();
    }

    function export_complete_test($test_id) {
        return $this->db->select('p.name as patient_name,t.*')
                        ->from('tests t')
                        ->where('id', $test_id)
                        ->join('patients p', 'p.id=t.patient_id')
                        ->get()->row_array();
    }

    function get_groups() {
        return $this->db->select('*')
                        ->from('subgroups')
                        ->where('status', 1)
                        ->order_by('id')
                        ->get()->result_array();
    }

    function get_patients() {
        return $this->db->select('*')
                        ->select('DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), birthdate)), "%Y")+0 as age', false)
                        ->from('patients')
                        ->where('status', 1)
                        ->order_by('name')
                        ->get()->result_array();
    }

    public function get_patient($id) {
        return $this->db->select('*')
                        ->from('patients')
                        ->where('id', $id)
                        ->where('status', 1)
                        ->get()->row_array();
    }

    function save_patient($data) {
        $this->db->set($data)
                ->insert('patients');
        if ($this->db->insert_id()) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function save_subgroup($data) {
        $this->db->set($data)
                ->insert('subgroups');
        if ($this->db->insert_id()) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function delete_subgroup($id) {
        $query = $this->db->set('status', 0)
                ->where('id', $id)
                ->update('subgroups');
        if ($query) {
            return "OK";
        } else {
            return "NOK";
        }
    }

    function delete_patient($id) {
        $query = $this->db->set('status', 0)
                ->where('id', $id)
                ->update('patients');
        if ($query) {
            return "OK";
        } else {
            return "NOK";
        }
    }

    function update_patient($data, $id) {
        $query = $this->db->set($data)
                ->where('id', $id)
                ->update('patients');
        if ($query) {
            return "OK";
        } else {
            return "NOK";
        }
    }

    function update_subgroup($data) {

        $this->db->set($data)
                ->insert('relationship_test_group');
        if ($this->db->insert_id()) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function get_users() {
        return $this->db->select('*')
                        ->from('users')
                        ->where('status', 1)
                        ->get()->result_array();
    }

    function save_user($data) {
        $this->db->set($data)
                ->insert('users');
        if ($this->db->insert_id()) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function delete_user($id) {
        $query = $this->db->set('status', 0)
                ->where('id', $id)
                ->update('users');
        if ($query) {
            return "OK";
        } else {
            return "NOK";
        }
    }

    function update_user($data, $id) {
        $query = $this->db->set($data)
                ->where('id', $id)
                ->update('users');
        if ($query) {
            return "OK";
        } else {
            return "NOK";
        }
    }

    function get_tests() {
        return $this->db->select('p.name as patient_name, u.name as user_name, t.*')
                        ->select('DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), p.birthdate)), "%Y")+0 as age', false)
                        ->select("date_format(t.realization_date,'%d/%m/%Y') as realization_date ", false)
                        ->from('tests t')
                        ->join('patients p', 'p.id=t.patient_id')
                        ->join('users u', 'u.id=t.user_id')
                        ->where('t.status', 1)
                        ->order_by('patient_name')
                        ->get()->result_array();
    }

    function get_patientgroup() {
        return $this->db->select('p.name as patient_name, u.name as user_name, t.*')
                        ->select('DATE_FORMAT(FROM_DAYS(DATEDIFF(NOW(), p.birthdate)), "%Y")+0 as age', false)
                        ->select("date_format(t.realization_date,'%d/%m/%Y') as realization_date ", false)
                        ->from('tests t')
                        ->join('patients p', 'p.id=t.patient_id')
                        ->join('users u', 'u.id=t.user_id')
                        ->where('t.status', 1)
                        ->order_by('patient_name')
                        ->get()->result_array();
    }

    function get_patientsubgroup($id) {
        return $this->db->select('t.*')
                        ->from('relationship_test_group t')
                        ->where('t.id_test', $id)
                        ->get()->result_array();
    }

    function save_test($data) {
        $this->db->set($data)
                ->insert('tests');
        if ($this->db->insert_id()) {
            return 'OK';
        } else {
            return 'NOK';
        }
    }

    function get_subgroups($cat) {
        return $this->db->select('*')
                        ->from('subgroups')
                        ->where('category_group', $cat)
                        ->where('status', 1)
                        ->get()->result_array();
    }

    function delete_test($id) {
        $query = $this->db->set('status', 0)
                ->where('id', $id)
                ->update('tests');
        if ($query) {
            return "OK";
        } else {
            return "NOK";
        }
    }

    function update_test_group($data, $id) {
        $query = $this->db->set('group', $data['group'])
                ->where('id', $id)
                ->update('tests');
        if ($query) {
            return "OK";
        } else {
            return "NOK";
        }
    }

}

?>