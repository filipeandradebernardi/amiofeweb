<?php

Class Auth extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * LOGIN
     * @param string $name or $email
     * @param string $password
     * @return string OK, NOTFOUND, ERROR
     */
    function login($login, $password) {
        $this->db->select('*')
                ->from('users')
                ->where('password', $password)
                ->where('status', 1)
                ->where('email ="' . $login . '"')
                ->limit(1);


        $query = $this->db->get();

        if ($query->result_id->num_rows === 1) {
            $row = $query->row();
            $msg = $row->id;
        } else {
            $this->db->select('*')
                    ->from('users')
                    ->where('password', $password)
                    ->where('status', 1)
                    ->where('name ="' . $login . '"')
                    ->limit(1);

            $query2 = $this->db->get();

            if ($query2->result_id->num_rows === 1) {
                $row2 = $query2->row();
                $msg = $row2->id;
            } else {
                $msg = "NOTFOUND";
            }
        }
        return $msg;
    }

}

?>