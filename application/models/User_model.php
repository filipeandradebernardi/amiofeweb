<?php

Class User_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('America/Sao_Paulo');
    }

    public function get_user($id) {
        return $this->db->select('*')
                        ->from('users')
                        ->where('id', $id)
                        ->where('status', 1)
                        ->get()->row_array();
    }

}

?>