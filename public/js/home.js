function delete_user(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/delete_user",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == "OK") {
                window.location = jQuery("body").data('baseurl') + 'home/list_users';
            }
        }
    });
}

function save_edited_user(id) {

    if (!jQuery("#name").val()) {
        jQuery('.name').addClass('has-error');
        return false;
    }
    if (!jQuery("#email").val()) {
        jQuery('.email').addClass('has-error');
        return false;
    }
    if (!jQuery("#password").val()) {
        jQuery('.password').addClass('has-error');
        return false;
    }

    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/update_user",
        type: "post",
        dataType: 'json',
        data: {
            name: jQuery("#name").val(),
            email: jQuery("#email").val(),
            password: jQuery("#password").val(),
            institution: jQuery("#institution").val(),
            permission: jQuery("#permission").val(),
            id: id
        },
        success: function (response) {
            if (response.status == "OK") {
                window.location = jQuery("body").data('baseurl') + 'home/list_users';
            }
        }
    });

}

function delete_patient(id) {
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/delete_patient",
        type: "post",
        dataType: 'json',
        data: {
            id: id
        },
        success: function (response) {
            if (response.status == "OK") {
                window.location = jQuery("body").data('baseurl') + 'home/list_patients';
            }
        }
    });
}

function save_edited_patient(id) {

    if (!jQuery("#name").val()) {
        jQuery('.name').addClass('has-error');
        return false;
    }
    if (!jQuery("#telephone").val()) {
        jQuery('.telephone').addClass('has-error');
        return false;
    }
    if (!jQuery("#city").val()) {
        jQuery('.city').addClass('has-error');
        return false;
    }
    if (!jQuery("#street").val()) {
        jQuery('.street').addClass('has-error');
        return false;
    }
    if (!jQuery("#number").val()) {
        jQuery('.number').addClass('has-error');
        return false;
    }
    if (!jQuery("#birthdate").val()) {
        jQuery('.birthdate').addClass('has-error');
        return false;
    }

    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/update_patient",
        type: "post",
        dataType: 'json',
        data: {
            name: jQuery("#name").val(),
            mother_name: jQuery("#mother_name").val(),
            telephone: jQuery("#telephone").val(),
            cellphone: jQuery("#cellphone").val(),
            city: jQuery("#city").val(),
            street: jQuery("#street").val(),
            number: jQuery("#number").val(),
            complement: jQuery("#complement").val(),
            gender: jQuery("#gender").val(),
            birthdate: jQuery("#birthdate").val(),
            id: id
        },
        success: function (response) {
            if (response.status == "OK") {
                window.location = jQuery("body").data('baseurl') + 'home/list_patients';
            }
        }
    });

}