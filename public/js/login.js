function login_enter(e) {
    if (e.keyCode == 13) {
        jQuery('#login_button').click();
        //login();
    }
}

function login() {
    jQuery('.login').removeClass('has-error');
    if (!jQuery("#username").val()) {
        jQuery('#username').addClass('has-error');
        return false;
    }

    if (!jQuery("#password").val()) {
        jQuery('#password').addClass('has-error');
        return false;
    }

    var check = false;
    if (jQuery("#RememberMe").is(":checked")) {
        check = true;
    }

    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "login/check_login",
        type: "post",
        dataType: 'json',
        data: {
            username: jQuery("#username").val(),
            password: jQuery("#password").val(),
            checkbox: check
        },
        success: function (response) {
            if (response.status == "OK") {
                window.location = jQuery("body").data('baseurl') + 'home';
            } else if (response.status == "ERROR") {
                jQuery('#btn_login').html('Entrar');
                var notice = new PNotify({
                    title: 'Dados de acesso',
                    text: 'Usuário ou senha inválidos!',
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            }
        }
    });
}
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function save_register() {
    if (!jQuery('#name').val()) {
        var notice = new PNotify({
            title: 'Nome',
            text: 'Corrija o campo de nome',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
    }
    //if (!jQuery('#email').val() || !validateEmail(jQuery("#email").val())) {
    if (!jQuery('#email').val()) {
        var notice = new PNotify({
            title: 'Email',
            text: 'Corrija o campo de email',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
    }
    if (!jQuery('#password').val()) {
        var notice = new PNotify({
            title: 'Nome',
            text: 'Corrija o campo de senha',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
    }
    if (jQuery('#password').val() !== jQuery('#password_confirmation').val()) {
        var notice = new PNotify({
            title: 'Nome',
            text: 'Senha e confirmação de senha estã diferentes',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        notice.get().click(function () {
            notice.remove();
        });
        return false;
    }
    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "home/save_register",
        type: "post",
        dataType: 'json',
        data: {
            name: jQuery('#name').val(),
            email: jQuery('#email').val(),
            password: jQuery('#password').val()
        },
        success: function (response) {
            if (response.status == 'NOK') {
                var notice = new PNotify({
                    title: response.title,
                    text: response.message,
                    type: 'error',
                    addclass: 'click-2-close',
                    hide: false,
                    buttons: {
                        closer: false,
                        sticker: false
                    }
                });
                notice.get().click(function () {
                    notice.remove();
                });
            } else {
                window.location = jQuery("body").data('baseurl') + 'home/index';
            }
        }
    });
}

function forgot_password() {
    if (!jQuery('#forgot_password_email').val()
            || !validateEmail(jQuery("#forgot_password_email").val())) {
        var notice = new PNotify({
            title: 'Campos vazio',
            text: 'O campo de email precisa estar preenchido',
            type: 'error',
            addclass: 'click-2-close',
            hide: false,
            buttons: {
                closer: false,
                sticker: false
            }
        });
        return false;
    }

    jQuery.ajax({
        url: jQuery("body").data("baseurl") + "login/forgot_password",
        type: "post",
        dataType: 'json',
        data: {
            email: jQuery('#forgot_password_email').val()
        },
        success: function (response) {
            var notice = new PNotify({
                title: response.title,
                text: response.message,
                type: response.type,
                addclass: 'click-2-close',
                hide: false,
                buttons: {
                    closer: false,
                    sticker: false
                }
            });
            notice.get().click(function () {
                notice.remove();
            });
        }
    });
}
